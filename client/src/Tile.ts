import * as PIXI from 'pixi.js'
import BaseSprite from './BaseSprite'
import { systemPixiApp, tileHeight, tileWidth } from './SystemMapGraphics'

export default class Tile extends BaseSprite {
  public system_x: number
  public system_y: number
  public originalTexture: PIXI.Texture
  constructor ({ x, y }: { x: number, y: number }) {
    const tileGraphic = new PIXI.Graphics()
    tileGraphic.lineStyle(5, 0xff0000).drawRect(x * tileWidth, y * tileHeight, tileWidth, tileHeight).lineStyle(0)
    const texture = systemPixiApp.renderer.generateTexture(tileGraphic)
    super(texture, undefined)
    this.originalTexture = texture
    this.position.set(x * tileWidth, y * tileHeight)
    this.system_x = x
    this.system_y = y
  }

  isInPath () {
    const container = new PIXI.Container()

    const circleGraphic = new PIXI.Graphics()
    circleGraphic.beginFill(0x000000).lineStyle(5, 0xffffff).drawCircle(tileWidth, tileHeight, tileHeight / 8).endFill()
    const circleTexture = systemPixiApp.renderer.generateTexture(circleGraphic)
    const circleSprite = new PIXI.Sprite(circleTexture)
    circleSprite.anchor.set(0.5)
    circleSprite.position.set(tileWidth / 2, tileHeight / 2)

    const tileGraphic = new PIXI.Graphics()
    tileGraphic.lineStyle(5, 0xff0000).drawRect(this.system_x * tileWidth, this.system_y * tileHeight, tileWidth, tileHeight).lineStyle(0)
    const tileTexture = systemPixiApp.renderer.generateTexture(tileGraphic)
    const tileSprite = new PIXI.Sprite(tileTexture)

    container.addChild(circleSprite)
    container.addChild(tileSprite)

    this.texture = systemPixiApp.renderer.generateTexture(container)
  }

  clearTravelPath () {
    this.texture = this.originalTexture
  }
}
