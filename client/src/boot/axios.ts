import axios, { AxiosInstance } from 'axios'
import { Notify } from 'quasar'
import { boot } from 'quasar/wrappers'

declare module 'vue/types/vue' {
  interface Vue {
    $axios: AxiosInstance;
  }
}

export default boot(({ Vue }) => {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  Vue.prototype.$axios = axios
  Vue.prototype.$axios.defaults.baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:3001/api/v1' : 'https://www.starwarsdynasties.com/api/v1'
  Vue.prototype.$axios.interceptors.response.use((response: Response) => response, (error: Error) => {
    console.log(error)
    Notify.create({
      type: 'negative',
      position: 'center',
      message: 'Application error',
      caption: 'see browser console for error detail',
    })
    return Promise.reject(error)
  })
})
