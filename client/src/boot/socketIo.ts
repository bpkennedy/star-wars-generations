import VueSocketIO from 'vue-socket.io'
// @ts-ignore
import SocketIO from 'socket.io-client'
import { boot } from 'quasar/wrappers'

export default boot(({ Vue, store }) => {
  Vue.use(new VueSocketIO({
    debug: process.env.NODE_ENV === 'development',
    connection: SocketIO(process.env.NODE_ENV === 'development' ? 'http://localhost:3001' : 'https://www.starwarsdynasties.com', {
      path: '/swdsocket',
      reconnection: true,
      reconnectionAttempts: Infinity,
      reconnectionDelay: 1500,
    }),
    vuex: {
      store,
      actionPrefix: 'SOCKET_ACTION_',
      mutationPrefix: 'SOCKET_MUTATION_'
    },
  }))
})
