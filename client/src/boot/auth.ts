// @ts-nocheck
/* eslint-disable */
import createAuth0Client from '@auth0/auth0-spa-js'
import { Loading } from 'quasar'

let instance
export const getInstance = () => instance

/** Define a default action to perform after authentication */
const DEFAULT_REDIRECT_CALLBACK = () => window.history.replaceState({}, document.title, window.location.pathname)

export default async ({ router, Vue, store }) => {
  /** Creates an instance of the Auth0 SDK. If one has already been created, it returns that instance */
  const useAuth0 = ({
                      onRedirectCallback = DEFAULT_REDIRECT_CALLBACK,
                      redirectUri = window.location.origin
                    }) => {
    if (instance) return instance

    // The 'instance' is simply a Vue object
    instance = new Vue({
      data() {
        return {
          loading: true,
          isAuthenticated: false,
          user: {},
          auth0Client: null,
          popupOpen: false,
          error: null
        }
      },
      methods: {
        /** Authenticates the user using a popup window */
        async loginWithPopup(o) {
          this.popupOpen = true

          try {
            if (this.auth0Client === null) {
              await this.auth0Client.loginWithPopup(o)
            }
          } catch (e) {
            // eslint-disable-next-line
            console.error(e)
          } finally {
            this.popupOpen = false
          }

          this.user = await this.auth0Client.getUser()
          this.isAuthenticated = true
        },
        /** Handles the callback when logging in using a redirect */
        async handleRedirectCallback() {
          this.loading = true
          try {
            await this.auth0Client.handleRedirectCallback()
            this.user = await this.auth0Client.getUser()
            this.isAuthenticated = true
          } catch (e) {
            this.error = e
          } finally {
            this.loading = false
          }
        },
        /** Authenticates the user using the redirect method */
        loginWithRedirect(o) {
          return this.auth0Client.loginWithRedirect(o)
        },
        /** Returns all the claims present in the ID token */
        getIdTokenClaims(o) {
          return this.auth0Client.getIdTokenClaims(o)
        },
        /** Returns the access token. If the token is invalid or missing, a new one is retrieved */
        getTokenSilently(o) {
          return this.auth0Client.getTokenSilently(o)
        },
        /** Gets the access token using a popup window */

        getTokenWithPopup(o) {
          return this.auth0Client.getTokenWithPopup(o)
        },
        /** Logs the user out and removes their session on the authorization server */
        logout(o) {
          return this.auth0Client.logout(o)
        },
        async checkAndLoadCharacter() {
          const characterResponse = await Vue.prototype.$axios.get('/character')
          if (characterResponse.status === 200) {
            Vue.set(store.state, 'character', characterResponse.data)
          }
        }
      },
      /** Use this lifecycle method to instantiate the SDK client */
      async created() {
        Loading.show({
          backgroundColor: 'dark'
        })
        this.auth0Client = await createAuth0Client({
          domain: 'dev-a4gfqbch.us.auth0.com',
          client_id: 'z7qPtVTYQb29IQ0HLOLyfWzITcVv0nJN',
          audience: 'starwarsdynastiesapi',
          redirect_uri: redirectUri,
          scope: 'read:all'
        })

        try {
          // If the user is returning to the app after authentication..
          if (
            window.location.search.includes("code=") &&
            window.location.search.includes("state=")
          ) {
            // handle the redirect and retrieve tokens
            const { appState } = await this.auth0Client.handleRedirectCallback()

            // Notify subscribers that the redirect callback has happened, passing the appState
            // (useful for retrieving any pre-authentication state)
            onRedirectCallback(appState)
          }
        } catch (e) {
          this.error = e
        } finally {
          // Initialize our internal authentication state
          this.isAuthenticated = await this.auth0Client.isAuthenticated()
          this.user = await this.auth0Client.getUser()
          if (this.isAuthenticated) {
            const accessToken = await this.getTokenSilently({
              audience: 'starwarsdynastiesapi',
              scope: 'read:all',
            })
            if (accessToken) {
              Vue.prototype.$axios.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken
              await this.checkAndLoadCharacter()
            }
          }
          this.loading = false
          Loading.hide()
        }
      }
    })

    return instance
  }

  const Auth0Plugin = {
    install(Vue, options) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      Vue.prototype.$auth = useAuth0(options)
    }
  }

  Vue.use(Auth0Plugin, {
    onRedirectCallback: appState => {
      router.push(
        appState && appState.targetUrl
          ? appState.targetUrl
          : window.location.pathname
      )
    }
  })
}
