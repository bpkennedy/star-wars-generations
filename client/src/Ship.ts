import * as PIXI from 'pixi.js'
import { Text, Container } from 'pixi.js'
import { addToViewport, directions, gridTileOffset, pixiFontStyle } from './PixiUtils'
import { tileWidth, tileHeight, systemViewport } from './SystemMapGraphics'
import Vehicle from 'src/Vehicle'
import Exhaust from 'src/Exhaust'
import { ease } from 'pixi-ease'
import store, { SYSTEM_SPRITE_CHANGED_ACTION } from 'src/store'

export default class Ship extends Container {
  private label: Text | null
  public vehicle: Vehicle
  public exhaust: Exhaust
  public system_x: number
  public system_y: number
  public swdId: number | undefined

  constructor ({ name, x = 0, y = 0, orientation = 0, swdId }: { name: string, x: number, y: number, orientation: number, swdId?: number | undefined }) {
    super()
    this.exhaust = new Exhaust({ name: exhaustType(), x, y })
    this.vehicle = new Vehicle({ name, x, y, swdId })
    this.label = null
    this.position.set(gridTileOffset(x) * tileWidth / 2, gridTileOffset(y) * tileHeight / 2)
    this.rotation = orientation
    this.name = name
    this.swdId = swdId
    this.system_x = x
    this.system_y = y
    this.pivot.x = gridTileOffset(x) * tileWidth / 2
    this.pivot.y = gridTileOffset(y) * tileHeight / 2
    this.addChild(this.exhaust)
    this.addChild(this.vehicle)
    addToViewport(systemViewport, this)
    this.exhaust.play()
  }

  containsPoint (point) {
    return this.vehicle.containsPoint(point)
  }

  createLabel (name: string, x: number, y: number) {
    this.label = new PIXI.Text(name, pixiFontStyle)
    this.label.x = x
    this.label.y = y
    this.label.anchor.set(0.5)
    addToViewport(systemViewport, this.label)
  }

  removeLabel () {
    if (this.label) {
      this.label.destroy()
    }
    this.label = null
  }

  async moveTo (x: number, y: number, rotation?: number | undefined) {
    const slide: any = ease.add(this, {
      x: gridTileOffset(x) * tileWidth / 2,
      y: gridTileOffset(y) * tileHeight / 2
    }, { duration: 1500, ease: 'easeOutQuad' })

    slide.once('complete', () => {
      if (rotation) {
        const rotate: any = ease.add(this, { rotation }, { duration: 1500, ease: 'easeOutQuad' })
        rotate.once('complete', () => {
          this.position.set(gridTileOffset(x) * tileWidth / 2, gridTileOffset(y) * tileHeight / 2)
          this.system_x = x
          this.system_y = y
        })
      } else {
        this.position.set(gridTileOffset(x) * tileWidth / 2, gridTileOffset(y) * tileHeight / 2)
        this.system_x = x
        this.system_y = y
      }
    })
    await store.dispatch(SYSTEM_SPRITE_CHANGED_ACTION, this)
  }

  orientToTravelDirection ([x, y]: number[]) {
    let direction = 'n'
    if (x > this.system_x && y < this.system_y) {
      direction = 'ne'
    } else if (x > this.system_x && y === this.system_y) {
      direction = 'e'
    } else if (x > this.system_x && y > this.system_y) {
      direction = 'se'
    } else if (x === this.system_x && y < this.system_y) {
      direction = 'n'
    } else if (x === this.system_x && y > this.system_y) {
      direction = 's'
    } else if (x < this.system_x && y < this.system_y) {
      direction = 'nw'
    } else if (x < this.system_x && y === this.system_y) {
      direction = 'w'
    } else if (x < this.system_x && y > this.system_y) {
      direction = 'sw'
    }
    this.rotation = directions[direction]
  }
}

function exhaustType () {
  return 'Normal_flight/Ship4_normal_flight'
}
