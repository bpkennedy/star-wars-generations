import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const SOCKET_ACTION_USERS_ONLINE = 'SOCKET_ACTION_USERS_ONLINE'

export const GALAXY_MAP_OPENED_ACTION = 'GALAXY_MAP_OPENED_ACTION'
export const SYSTEM_GRID_OPENED_ACTION = 'SYSTEM_GRID_OPENED_ACTION'
export const SYSTEM_TRIPS_CHANGED_ACTION = 'SYSTEM_TRIPS_CHANGED_ACTION'
export const PLAYER_TRAVELING_ACTION = 'PLAYER_TRAVELING_ACTION'
export const FINISHED_PLAYER_SHIP_MOVE_ACTION = 'FINISHED_PLAYER_SHIP_MOVE_ACTION'
export const SYSTEM_SPRITE_RENDERED_ACTION = 'SYSTEM_SPRITES_CHANGED_ACTION'
export const SYSTEM_SPRITE_CHANGED_ACTION = 'SYSTEM_SPRITE_CHANGED_ACTION'
export const CHAT_TOGGLED_ACTION = 'CHAT_TOGGLED_ACTION'

const SET_PLANETS_MUTATION = 'SET_PLANETS_MUTATION'
const CLOSE_CHAT_DRAWER_MUTATION = 'CLOSE_CHAT_DRAWER_MUTATION'
const OPEN_CHAT_DRAWER_MUTATION = 'OPEN_CHAT_DRAWER_MUTATION'
const SET_PLAYER_SHIP_MUTATION = 'SET_PLAYER_SHIP_MUTATION'
const SET_SYSTEM_MUTATION = 'SET_SYSTEM_MUTATION'
const SET_OTHER_SHIPS_MUTATION = 'SET_OTHER_SHIPS_MUTATION'
const SET_TRIPS_MUTATION = 'SET_TRIPS_MUTATION'
const ADD_SYSTEM_SPRITE_MUTATION = 'ADD_SYSTEM_SPRITE_MUTATION'
const UPDATE_SYSTEM_SPRITE_MUTATION = 'UPDATE_SYSTEM_SPRITE_MUTATION'
const SET_PLAYER_TRAVELING_MUTATION = 'SET_PLAYER_TRAVELING_MUTATION'

export const SYSTEM_TRIP_DESTINATION_GETTER = 'SYSTEM_TRIP_DESTINATION_GETTER'
export const PLAYER_CURRENT_SHIP_SYSTEM_POSITION_GETTER = 'PLAYER_CURRENT_SHIP_SYSTEM_POSITION_GETTER'

export default new Vuex.Store({
  state: {
    chatDrawerOpen: false,
    character: null as any,
    systemSpriteContainers: [],
    currentPlayerShip: null,
    otherShips: [],
    usersOnline: 0,
    planets: [],
    trips: [],
    system: {},
    playerTraveling: false,
  },
  actions: {
    [SYSTEM_SPRITE_RENDERED_ACTION] ({ commit }, systemSprite) {
      commit(ADD_SYSTEM_SPRITE_MUTATION, systemSprite)
    },
    [SYSTEM_SPRITE_CHANGED_ACTION] ({ commit }, sprite) {
      commit(UPDATE_SYSTEM_SPRITE_MUTATION, sprite)
    },
    [PLAYER_TRAVELING_ACTION] ({ commit }, isTraveling: boolean) {
      commit(SET_PLAYER_TRAVELING_MUTATION, isTraveling)
    },
    async [SYSTEM_TRIPS_CHANGED_ACTION] ({ state, dispatch, commit }, systemTrips) {
      commit(SET_TRIPS_MUTATION, systemTrips)
      const playerTrip = systemTrips.find(t => t.ship_id === state.currentPlayerShip.id)
      if (playerTrip) {
        const onLastX = playerTrip.path[playerTrip.path.length - 1][0] === playerTrip.current_tile[0]
        const onLastY = playerTrip.path[playerTrip.path.length - 1][1] === playerTrip.current_tile[1]
        if (onLastX && onLastY) {
          await dispatch(FINISHED_PLAYER_SHIP_MOVE_ACTION)
        }
        moveShips(state)
      }
    },
    async [GALAXY_MAP_OPENED_ACTION] ({ state, commit }) {
      if (state.planets.length > 0) {
        return
      }
      const planetsResponse = await Vue.prototype.$axios.get('/planets')
      commit(SET_PLANETS_MUTATION, planetsResponse.data)
    },
    async [FINISHED_PLAYER_SHIP_MOVE_ACTION] ({ state, dispatch, commit }) {
      await dispatch(PLAYER_TRAVELING_ACTION, false)
      const characterShip = await Vue.prototype.$axios.get('/ships/' + state.character.ship_id)
      commit(SET_PLAYER_SHIP_MUTATION, characterShip.data)
    },
    async [SYSTEM_GRID_OPENED_ACTION] ({ state, commit }) {
      if (state.character !== null) {
        const characterShip = await Vue.prototype.$axios.get('/ships/' + state.character.ship_id)
        commit(SET_PLAYER_SHIP_MUTATION, characterShip.data)
        const characterSystem = await Vue.prototype.$axios.get('/systems/' + characterShip.data.system_id)
        commit(SET_SYSTEM_MUTATION, characterSystem.data)
        const systemShips = await Vue.prototype.$axios.get('/systems/' + characterShip.data.system_id + '/ships')
        commit(SET_OTHER_SHIPS_MUTATION, systemShips.data)
      }
    },
    [CHAT_TOGGLED_ACTION] ({ state, commit }) {
      if (state.chatDrawerOpen) {
        commit(CLOSE_CHAT_DRAWER_MUTATION)
      } else {
        commit(OPEN_CHAT_DRAWER_MUTATION)
      }
    },
    [SOCKET_ACTION_USERS_ONLINE] ({ state }, { count }) {
      Vue.set(state, 'usersOnline', count)
    }
  },
  mutations: {
    [SET_PLANETS_MUTATION] (state, planets) {
      Vue.set(state, 'planets', planets)
    },
    [CLOSE_CHAT_DRAWER_MUTATION] (state) {
      Vue.set(state, 'chatDrawerOpen', false)
    },
    [OPEN_CHAT_DRAWER_MUTATION] (state) {
      Vue.set(state, 'chatDrawerOpen', true)
    },
    [SET_PLAYER_SHIP_MUTATION] (state, ship) {
      Vue.set(state, 'currentPlayerShip', ship)
    },
    [SET_SYSTEM_MUTATION] (state, system) {
      Vue.set(state, 'system', system)
    },
    [SET_TRIPS_MUTATION] (state, systemTrips) {
      Vue.set(state, 'trips', systemTrips)
    },
    [ADD_SYSTEM_SPRITE_MUTATION] (state, systemSprite) {
      state.systemSpriteContainers.push(systemSprite)
    },
    [UPDATE_SYSTEM_SPRITE_MUTATION] (state, updatedSprite) {
      state.systemSpriteContainers = [
        ...state.systemSpriteContainers.filter(s => s.swdId !== updatedSprite.swdId),
        updatedSprite
      ]
    },
    [SET_PLAYER_TRAVELING_MUTATION] (state, isTraveling) {
      Vue.set(state, 'playerTraveling', isTraveling)
    },
    [SET_OTHER_SHIPS_MUTATION] (state, otherShips) {
      Vue.set(state, 'otherShips', otherShips.filter((s: any) => {
        if (state.character !== null) {
          return s.owner_character_id !== state.character.id
        }
        return true
      }))
    }
  },
  getters: {
    [SYSTEM_TRIP_DESTINATION_GETTER]: state => {
      const playerTrip = state.trips.find(t => t.ship_id === state.currentPlayerShip.id)
      if (playerTrip) {
        return playerTrip.path[playerTrip.path.length - 1].join(',')
      }
      return null
    },
    [PLAYER_CURRENT_SHIP_SYSTEM_POSITION_GETTER]: state => {
      return [state.currentPlayerShip.system_x, state.currentPlayerShip.system_y].join(',')
    }
  },
  modules: {}
})

function moveShips (state) {
  for (const trip of state.trips) {
    const nextMoveX = trip.current_tile[0]
    const nextMoveY = trip.current_tile[1]
    const rotationPoint = getNextIndex(trip.path, [nextMoveX, nextMoveY])
    const tripShip = state.systemSpriteContainers.find(t => t.swdId === trip.ship_id)
    if (tripShip) {
      tripShip.moveTo(nextMoveX, nextMoveY, rotationPoint, trip.rotation)
    }
  }
}

function getNextIndex (array, point) {
  const index = array.findIndex(i => i === point)
  return array[index + 1] ? array[index + 1] : array[index]
}
