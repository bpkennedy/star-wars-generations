import * as PIXI from 'pixi.js'
import BaseSprite from './BaseSprite'
import { galaxyPixiApp, worldWidth, worldHeight } from './GalaxyMapGraphics'

export default class Planet extends BaseSprite {
  constructor ({ planetData }: { planetData: any, swdId: number | undefined }) {
    const planetGraphic = new PIXI.Graphics()
    planetGraphic.beginFill(0xffffff).lineStyle(0).drawCircle(0, 0, 2).endFill()
    planetGraphic.tint = addRegionColor(planetData)
    const texture = galaxyPixiApp.renderer.generateTexture(planetGraphic)
    super(texture, planetData.id)
    this.position.set(planetData.x + (worldWidth / 2), planetData.y + (worldHeight / 2))
  }
}

const regionColorMap: any = {}

function addRegionColor (planet: any) {
  if (regionColorMap[planet.region]) {
    return regionColorMap[planet.region].color
  } else {
    const color = Math.floor(Math.random() * 0xffffff)
    regionColorMap[planet.region] = { color }
    return color
  }
}
