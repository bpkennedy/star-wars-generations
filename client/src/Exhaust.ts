import * as PIXI from 'pixi.js'
import { directions, gridTileOffset } from './PixiUtils'
import { tileWidth, tileHeight, exhaustSheet } from './SystemMapGraphics'

export default class Exhaust extends PIXI.AnimatedSprite {
  constructor ({ name, x = 0, y = 0 }: { name: string, x: number, y: number }) {
    super(exhaustSheet.spritesheet.animations[`${name}`])
    this.scale.x = 1.5
    this.scale.y = 1.5
    this.position.set(gridTileOffset(x) * tileWidth / 2, gridTileOffset(y) * tileHeight / 2)
    this.rotation = directions.w
    this.anchor.set(1.9, 0.48)
    this.animationSpeed = 0.15
  }
}
