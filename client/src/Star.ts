import BaseSprite from './BaseSprite'
import { gridTileOffset } from './PixiUtils'
import { systemTextures, tileWidth, tileHeight } from './SystemMapGraphics'

function starColor (x: number, y: number) {
  if (x > (tileWidth / 2) && y < (tileHeight / 2)) {
    return 'star_blue04'
  }
  if (x > (tileWidth / 2) && y > (tileHeight / 2)) {
    return 'star_red04'
  }
  if (x < (tileWidth / 2) && y < (tileHeight / 2)) {
    return 'star_yellow04'
  }
  if (x < (tileWidth / 2) && y < (tileHeight / 2)) {
    return 'star_white04'
  }
}

export default class Star extends BaseSprite {
  public system_x: number
  public system_y: number
  constructor ({ x = 0, y = 0 }: { x: number, y: number }) {
    super(systemTextures[`${starColor(x, y)}.png`], undefined)
    this.scale.x = 0.25
    this.scale.y = 0.25
    this.position.set(gridTileOffset(x) * tileWidth / 2, gridTileOffset(y) * tileHeight / 2)
    this.anchor.set(0.5)
    this.system_x = x
    this.system_y = y
  }
}
