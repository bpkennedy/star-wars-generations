import * as PIXI from 'pixi.js'
import sound from 'pixi-sound'
import { Viewport } from 'pixi-viewport'
import { addToViewport, snapToPoint } from './PixiUtils'
import store from './store'
import Station from './Station'
import Star from './Star'
import Tile from './Tile'

export let systemPixiApp = null
export let systemViewport = null
export const worldWidth = 2164
export const worldHeight = 1976
export const rows = 10
export const columns = 10
export const tileWidth = worldWidth / columns
export const tileHeight = worldHeight / rows
export let textures = null
export let systemTextures = null
export let exhaustSheet = null
// export let thrusters = null

export function loadTextureAtlas () {
  const assetLoader = new PIXI.Loader()
  return new Promise((resolve) => {
    PIXI.utils.clearTextureCache()
    assetLoader
      .add('atlas', 'tiles/atlas.json')
      .add('systemAtlas', 'tiles/systemAtlas.json')
      .add('exhaustAtlas', 'tiles/swdExhaust.json')
      .add('thrusters', 'sounds/thrusters.mp3')
      .load((loader, resources) => {
        textures = resources.atlas.textures
        systemTextures = resources.systemAtlas.textures
        exhaustSheet = resources.exhaustAtlas
        // thrusters = sound.Sound.from({
        //   source: 'sounds/thrusters.mp3',
        //   volume: adjustVolume,
        // })
        resolve()
      })
  })
}

export function createSystemPixiApp () {
  systemPixiApp = new PIXI.Application({ resizeTo: window, transparent: true })
  document.getElementById('grid').appendChild(systemPixiApp.view)
  systemPixiApp.view.style.textAlign = 'center'
}

export function createStarSystemGrid () {
  // draw tilemap outline
  const gridOutline = systemViewport.addChild(new PIXI.Graphics())
  gridOutline.lineStyle(5, 0xff0000).drawRect(0, 0, worldWidth, worldHeight).lineStyle(0)

  // draw individual tiles
  const tileMatrix = store.state.system.tile_matrix
  const tiles = []
  for (let r = 0; r < tileMatrix.length; r++) {
    for (let c = 0; c < tileMatrix[r].length; c++) {
      if (tileMatrix[r][c] === 1) {
        tiles.unshift(stationSprite(c, r))
      } else if (tileMatrix[r][c] === 2) {
        tiles.unshift(starSprite(c, r))
      }
      // otherwise empty square
      tiles.unshift(tileSprite(c, r))
    }
  }
  const currentPlayerShip = store.state.currentPlayerShip
  snapToPoint(systemViewport, currentPlayerShip.system_x, currentPlayerShip.system_y, worldWidth, worldHeight)
  return tiles
}

function starSprite (posX, posY) {
  const starSprite = new Star({ x: posX, y: posY })
  addToViewport(systemViewport, starSprite)
  return starSprite
}

function stationSprite (posX, posY) {
  const stationSprite = new Station({ x: posX, y: posY })
  addToViewport(systemViewport, stationSprite)
  return stationSprite
}

function tileSprite (x, y) {
  const tileSprite = new Tile({ x, y })
  addToViewport(systemViewport, tileSprite)
  return tileSprite
}

function adjustSystemVolume (event) {
  sound.volumeAll = Math.max(0,
    Math.min(1, parseFloat(event.scaled) / 4)
  )
}

export function createSystemViewport (callback) {
  systemViewport = systemPixiApp.stage.addChild(new Viewport({
    screenWidth: window.innerWidth,
    screenHeight: window.innerHeight,
    worldWidth,
    worldHeight,
    interaction: systemPixiApp.renderer.plugins.interaction // the interaction module is important for wheel to work properly when renderer.view is placed or scaled
  }).drag().pinch().wheel().decelerate()).on('clicked', callback).on('zoomed-end', adjustSystemVolume)
}
