import BaseSprite from './BaseSprite'
import { gridTileOffset } from './PixiUtils'
import { textures, tileWidth, tileHeight } from './SystemMapGraphics'

export default class Vehicle extends BaseSprite {
  constructor ({ name, x = 0, y = 0, swdId }: { name: string, x: number, y: number, swdId?: number | undefined }) {
    super(textures[`${name}.png`], swdId)
    this.scale.x = 0.25
    this.scale.y = 0.25
    this.position.set(gridTileOffset(x) * tileWidth / 2, gridTileOffset(y) * tileHeight / 2)
    this.anchor.set(0.5)
  }
}
