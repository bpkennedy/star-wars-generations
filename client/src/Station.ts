import BaseSprite from './BaseSprite'
import { addToViewport, directions, gridTileOffset, pixiFontStyle } from './PixiUtils'
import { systemTextures, tileWidth, tileHeight, systemViewport } from './SystemMapGraphics'
import * as PIXI from 'pixi.js'
import { Text } from 'pixi.js'

export default class Station extends BaseSprite {
  private label: Text | null
  public system_x: number
  public system_y: number
  constructor ({ x = 0, y = 0, swdId }: { x: number, y: number, swdId?: number | undefined }) {
    super(systemTextures['Station1.png'], swdId)
    this.scale.x = 0.4
    this.scale.y = 0.4
    this.position.set(gridTileOffset(x) * tileWidth / 2, gridTileOffset(y) * tileHeight / 2)
    this.anchor.set(0.5)
    this.rotation = directions.n
    this.name = 'Orbital Station'
    this.label = null
    this.system_x = x
    this.system_y = y
  }

  createLabel (name: string, x: number, y: number) {
    this.label = new PIXI.Text('Orbital Station', pixiFontStyle)
    this.label.x = x
    this.label.y = y
    this.label.anchor.set(0.5)
    addToViewport(systemViewport, this.label)
  }

  removeLabel () {
    if (this.label) {
      this.label.destroy()
    }
    this.label = null
  }
}
