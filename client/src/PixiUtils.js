export function snapToCenter (appViewport, worldWidth, worldHeight) {
  appViewport.snap(0 + (worldWidth / 2), 0 + (worldHeight / 2), {
    removeOnComplete: true,
    removeOnInterrupt: true,
    forceStart: true,
  })
  appViewport.snapZoom({
    width: window.innerWidth * 2,
    height: window.innerHeight * 2,
    removeOnComplete: true,
    removeOnInterrupt: true,
  })
}

export function snapToPoint (appViewport, x, y, worldWidth, worldHeight) {
  appViewport.snap(x * (worldWidth / 10), y * (worldHeight / 10), {
    removeOnComplete: true,
    removeOnInterrupt: true,
    forceStart: true,
  })
  appViewport.snapZoom({
    width: window.innerWidth * 1.5,
    height: window.innerHeight * 1.5,
    removeOnComplete: true,
    removeOnInterrupt: true,
  })
}

export const pixiFontStyle = {
  font: 'bold italic 36px Arial',
  fontFamily: 'ItcSerifGothicBold',
  fill: '#F7EDCA',
  stroke: '#4a1850',
  strokeThickness: 5,
  dropShadow: true,
  dropShadowColor: '#000000',
  dropShadowAngle: Math.PI / 6,
  dropShadowDistance: 6,
  wordWrap: true,
  wordWrapWidth: 440
}

export function addToViewport (appViewport, item) {
  appViewport.addChild(item)
}

export const directions = {
  n: 0,
  ne: Math.PI / 4,
  e: Math.PI / 2,
  se: (3 * Math.PI) / 4,
  s: Math.PI,
  sw: (5 * Math.PI) / 4,
  w: (3 * Math.PI) / 2,
  nw: (7 * Math.PI) / 4,
}

export function gridTileOffset (tileIndex) {
  return tileIndex + tileIndex + 1
}
