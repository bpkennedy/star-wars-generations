import * as PIXI from 'pixi.js'
import { Viewport } from 'pixi-viewport'
import { addToViewport, snapToPoint } from './PixiUtils'
import Planet from './Planet'
import store from './store'

export let galaxyPixiApp = null
export let galaxyViewport = null
export const worldWidth = 2164
export const worldHeight = 1976

export function createGalaxyPixiApp () {
  galaxyPixiApp = new PIXI.Application({ resizeTo: window, transparent: true })
  document.getElementById('map').appendChild(galaxyPixiApp.view)
  galaxyPixiApp.view.style.textAlign = 'center'
}

export function createGalaxyViewport (callback) {
  galaxyViewport = galaxyPixiApp.stage.addChild(new Viewport({
    screenWidth: window.innerWidth,
    screenHeight: window.innerHeight,
    worldWidth,
    worldHeight,
    interaction: galaxyPixiApp.renderer.plugins.interaction // the interaction module is important for wheel to work properly when renderer.view is placed or scaled
  }).drag().pinch().wheel().decelerate()).on('clicked', callback)
}

export function createGalaxyPlot () {
  // draw map outline
  const gridOutline = galaxyViewport.addChild(new PIXI.Graphics())
  gridOutline.lineStyle(5, 0xff0000).drawRect(0, 0, worldWidth, worldHeight).lineStyle(0)

  const dataPlanets = store.state.planets
  const planets = []

  for (let i = 0; i < dataPlanets.length; i++) {
    const planet = new Planet({ planetData: dataPlanets[i], swdId: dataPlanets[i].id })
    addToViewport(galaxyViewport, planet)
    planets.push(planet)
  }
  const system = store.state.system
  snapToPoint(galaxyViewport, system.x, system.y, worldWidth, worldHeight)
  return planets
}
