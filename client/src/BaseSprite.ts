import * as PIXI from 'pixi.js'

export default class BaseSprite extends PIXI.Sprite {
  public swdId: number | undefined
  constructor (texture: any, swdId: any) {
    super(texture)
    this.swdId = swdId
  }
}
