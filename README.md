Star Wars Generations
---------------------

### Prereqs
* Docker
* node 12.11+

### Develop
Just run `./local-start.sh` to start the containers.
* For cleaning caches: `docker system prune --volumes` and `docker-compose build --no-cache`
* To view running docker containers: `docker ps`
* To shell into a docker container to view its output/use: `docker exec -it swg-api sh`

The UI runs on port 3000.  The api on port 3001.