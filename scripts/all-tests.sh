#!/bin/bash
if [ -z "$1" ]; then
        ./scripts/local-db.sh &
elif [ "$1" != "--skip-reset" ]; then
        echo Bad argument! Try --skip-reset
        exit 1
fi

until docker exec postgres psql -U postgres -c "select 1" > /dev/null 2>&1; do sleep 2; done

cd server
npm test &
api_pid=$!
cd -

cd client
npm run lint &
ui_pid=$!
cd -

wait $api_pid
api_result=$?
wait $ui_pid
ui_result=$?

if [ $api_result -eq 0 ] && [ $ui_result -eq 0 ]; then
	exit 0
else
	if [ $api_result -ne 0 ]; then
		echo "API unit tests failed"
	fi
	if [ $ui_result -ne 0 ]; then
		echo "UI linting failed"
	fi
	exit 1
fi
