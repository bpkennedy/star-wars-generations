#!/bin/bash

docker ps -a | grep postgres | awk '{print $1;}' | xargs docker stop
docker ps -a | grep postgres | awk '{print $1;}' | xargs docker rm
docker run --name postgres \
  -e POSTGRES_PASSWORD='postgres' \
  -p '25432:5432' \
  postgres:latest
