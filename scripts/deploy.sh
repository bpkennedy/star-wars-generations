#!/bin/bash

# stop any current running containers
docker-compose down --remove-orphans

# build images
docker-compose -f docker-compose.build.yml build

# push images to docker
docker push 'bpkennedy/swd:swg-client-latest'
docker push 'bpkennedy/swd:swg-api-latest'

# pull docker images on remote host
docker-compose -H "ssh://root@116.203.42.239" -f docker-compose.yml pull

# start containers on the remote host
docker-compose -H "ssh://root@116.203.42.239" -f docker-compose.yml up --build -d
