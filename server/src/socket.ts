import { retrieveCharacterWithDynasty } from './api/character-api'
import * as logger from 'loglevel'
export let io: any

export const initSockets = (server: any) => {
    io = require('socket.io').listen(server, {
        path: '/swdsocket'
    })
    io.on('connection', (client: any) => {
        broadcastUsersOnline('connected')
        client.on('chatMessage', async ({ message, date, uid }: { message: string, date: any, uid: string }) => {
            const characterWithDynasty: any = await retrieveCharacterWithDynasty(uid)
            const characterMessage = {
                tempId: Date.now(),
                message,
                date,
                handle: characterWithDynasty.name + ' ' + characterWithDynasty.dynasty.dynasty_name,
            }
            io.emit('newChatMessage', characterMessage)
            logger.warn(`user ${uid} created a chat message`)
        })
        client.on('join', (systemId: number) => {
            client.join(systemId)
        })
        client.on('disconnect', () => {
            broadcastUsersOnline('disconnected')
        })
    })
}

function broadcastUsersOnline(action: string) {
    io.of('/').clients((error: any, clients: any) => {
        if (error) throw error
        io.emit('USERS_ONLINE', { count: clients.length })
        logger.warn(`User ${action}`)
    })
}