module.exports = {
    pgUser: process.env.POSTGRES_USER || 'postgres',
    pgHost: process.env.POSTGRES_HOST || 'localhost',
    pgDatabase: process.env.POSTGRES_DB || 'postgres',
    pgPassword: process.env.POSTGRES_PASSWORD || 'postgres',
    pgPort: process.env.POSTGRES_PORT || '25432'
}
