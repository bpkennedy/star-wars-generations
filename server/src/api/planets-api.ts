import express from 'express'
import {Planet} from '../dao/planets-dao'
import {Resource} from '../dao/resources-dao'
import {Company} from '../dao/companies-dao'
import {
    calculateDecrementResourceQuantity,
    calculateIncrementResourceQuantity,
    calculatePopulation,
    calculateQualityOfLife,
} from '../formulas'

export async function getAllPlanets(req: express.Request, res: express.Response) {
    const items = await Planet.query().where('deleted', false)
    res.status(200).send(items)
}

export async function getPlanet(req: express.Request, res: express.Response) {
    const planet = await Planet.query().findById(req.params.id)
                            .where('deleted', false)
                            .withGraphFetched('[resources, companies]')
    res.status(200).send(planet || {})
}

export async function deletePlanet(req: express.Request, res: express.Response) {
    await Planet.query().findById(req.params.id).patch({
        deleted: true,
    })
    res.status(204).send({})
}

export async function updatePlanets() {
    const allCompanyFranchises = await Company.query().where('deleted', false)
    const planets = await Planet.query().where('deleted', false)

    for (const planet of planets) {
        await incrementResources(planet.id)
        await depleteResources(planet.id, allCompanyFranchises)
    }

    return Planet.query().where('deleted', false).withGraphFetched('[resources, companies]')
}

async function incrementResources(planetId: number) {
    const planet: any = await Planet.query().findById(planetId).withGraphFetched('[resources]')
    const resources = planet.resources
    for (const resource of resources) {
        const resourcesToAdd = calculateIncrementResourceQuantity(resource.quantity, planet.climate)
        await Resource.query().findById(resource.id).patch({
            quantity: resource.quantity + resourcesToAdd
        })
    }
}

async function depleteResources(planetId: number, allCompanyFranchises: Company[]) {
    const planet: any = await Planet.query().findById(planetId).withGraphFetched('[resources, companies]')
    const resources = planet.resources
    const planetCompanies = planet.companies
    for (const resource of resources) {
        const resourcesToRemove = calculateDecrementResourceQuantity(resource, planetCompanies, allCompanyFranchises)
        const newResourceAmount = resource.quantity - resourcesToRemove
        await Resource.query().findById(resource.id).patch({
            quantity: newResourceAmount > 0 ? newResourceAmount : 0
        })
    }
}

export async function updatePlanetAttributes(planetId: number, increment: boolean = true) {
    const planet: Planet = await Planet.query().findById(planetId)
    const updatedPlanet = calculateClimateCrimeJobProspects(planet, increment)
    const planetWithAttributes = {
        ...updatedPlanet,
        qol: calculateQualityOfLife(updatedPlanet),
    }
    await Planet.query().findById(planetId).patch({
        ...planetWithAttributes,
        population: calculatePopulation(planet.population, planetWithAttributes.qol)
    })
}

function calculateClimateCrimeJobProspects(planet: {climate: number, crime: number, job_prospects: number}, increment: boolean) {
    if (increment) {
        return {
            climate: planet.climate - (planet.climate * .02),
            crime: planet.crime + (planet.crime * .05),
            job_prospects: planet.job_prospects + (planet.job_prospects * .02),
        }
    } else {
        return {
            climate: planet.climate + (planet.climate * .02),
            crime: planet.crime - (planet.crime * .05),
            job_prospects: planet.job_prospects - (planet.job_prospects * .02),
        }
    }

}
