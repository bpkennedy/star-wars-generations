import express from 'express'
import {GameState} from '../dao/game-state-dao'

export async function getHeartbeat(req: express.Request, res: express.Response) {
    const gameState = await GameState.query().findById(1)
    res.status(200).send(gameState)
}