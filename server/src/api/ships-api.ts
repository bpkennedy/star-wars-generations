import express from 'express'
import {Ship} from '../dao/ships-dao'
import {getItem} from '../util'

export const baseTravelRate: { fighter: number} = {
    fighter: 2
}

export async function getAllShips(req: express.Request, res: express.Response) {
    const items = await Ship.query().where('deleted', false)
    res.status(200).send(items)
}

export async function getShip(req: express.Request, res: express.Response) {
    const item = await getItem(Ship, parseInt(req.params.id, 10))
    res.status(200).send(item || {})
}

export async function deleteShip(req: express.Request, res: express.Response) {
    await Ship.query().findById(req.params.id).patch({
        deleted: true,
    })
    res.status(204).send({})
}

export async function updateShipPositions(updatedTrips: any[]) {
    for (const trip of updatedTrips) {
        if (!trip.rotation) {
            const ship = await Ship.query().findById(trip.ship_id).limit(1).first()
            await Ship.query().findById(trip.ship_id).patch({
                system_x: trip.current_tile[0],
                system_y: trip.current_tile[1],
                rotation: ship.rotation
            })
        } else {
            await Ship.query().findById(trip.ship_id).patch({
                system_x: trip.current_tile[0],
                system_y: trip.current_tile[1],
                rotation: trip.rotation
            })
        }
    }
}