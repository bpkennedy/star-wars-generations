import express from 'express'
import {Trip} from '../dao/trips-dao'
import {getItem} from '../util'
import * as _ from 'lodash'
import {retrieveCharacterWithDynasty} from './character-api'
import {Ship} from '../dao/ships-dao'
import {baseTravelRate} from './ships-api'
// @ts-ignore
import {Clock} from '../clock'

export async function getSystemTrips(req: express.Request, res: express.Response) {
    const items = await Trip.query().where('system_id', req.params.id).andWhere('deleted', false)
    res.status(200).send(items.map((t: Trip) => _.omit(t, ['deleted'])) || [])
}

export async function createTrip(req: any, res: express.Response) {
    const { path } = req.body
    const characterWithDynasty = await retrieveCharacterWithDynasty(req.user.sub)
    const characterShip = await getItem(Ship, characterWithDynasty.ship_id)
    // @ts-ignore
    // TODO: instead of flat value, create formula of char skill and engine stat to get rate (r) - ultimately Tiles Per Hour units
    const tilesPerMinute = baseTravelRate[characterShip.class]
    const travelTimeInMinutes = (path.length - 1) / tilesPerMinute
    const start_date = Clock.currentDate()
    const destination_date = new Date(start_date.getTime() + travelTimeInMinutes * 60000)

    const newTrip = await Trip.query().insert({
        path,
        start_date,
        destination_date,
        ship_id: characterShip.id,
        system_id: characterShip.system_id,
        deleted: false
    })
    res.status(201).send(_.omit(newTrip, ['deleted']))
}

export async function expireCompletedTrips() {
    const allActiveTrips = await Trip.query().where('deleted', false)
    for (const trip of allActiveTrips) {
        if (trip.destination_date < Clock.currentDate()) {
            await Trip.query().findById(trip.id).patch({ deleted: true })
        }
    }
}

export function updateTripPositions(trips: any[]) {
    return trips.map(t => {
        const minutesTraveled: number = (Math.abs(new Date(t.start_date).getTime() - Clock.currentDate()) / 1000) / 60
        const totalDurationInMinutes: number = (Math.abs(new Date(t.start_date).getTime() - new Date(t.destination_date).getTime()) / 1000) / 60
        const tripCompletePercent = minutesTraveled / totalDurationInMinutes
        const nearestWholePoint = Math.floor((t.path.length - 1) * tripCompletePercent)
        return {
            ...t,
            current_tile: t.path[nearestWholePoint],
            rotation: orientToDirection(t.path[nearestWholePoint], t.path[nearestWholePoint + 1])
        }
    })
}

const directions: any = {
    n: 0,
    ne: Math.PI / 4,
    e: Math.PI / 2,
    se: (3 * Math.PI) / 4,
    s: Math.PI,
    sw: (5 * Math.PI) / 4,
    w: (3 * Math.PI) / 2,
    nw: (7 * Math.PI) / 4,
}

function orientToDirection(prevPoint: number[], nextPoint: number[]) {
    // no nextPoint if we are on last point of path, don't change orientation
    if (!nextPoint) {
        return
    }
    let direction: string = 'n'
    if (nextPoint[0] > prevPoint[0] && nextPoint[1] < prevPoint[1]) {
        direction = 'ne'
    } else if (nextPoint[0] > prevPoint[0] && nextPoint[1] === prevPoint[1]) {
        direction = 'e'
    } else if (nextPoint[0] > prevPoint[0] && nextPoint[1] > prevPoint[1]) {
        direction = 'se'
    } else if (nextPoint[0] === prevPoint[0] && nextPoint[1] < prevPoint[1]) {
        direction = 'n'
    } else if (nextPoint[0] === prevPoint[0] && nextPoint[1] > prevPoint[1]) {
        direction = 's'
    } else if (nextPoint[0] < prevPoint[0] && nextPoint[1] < prevPoint[1]) {
        direction = 'nw'
    } else if (nextPoint[0] < prevPoint[0] && nextPoint[1] === prevPoint[1]) {
        direction = 'w'
    } else if (nextPoint[0] < prevPoint[0] && nextPoint[1] > prevPoint[1]) {
        direction = 'sw'
    }
    return directions[direction]
}
