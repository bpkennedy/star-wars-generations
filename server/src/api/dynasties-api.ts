import express from 'express'
import {Dynasty} from '../dao/dynasties-dao'
import * as _ from 'lodash'

export async function getAllDynasties(req: express.Request, res: express.Response) {
    const items = await Dynasty.query().where('deleted', false)
    res.status(200).send(items.map(i => _.omit(i, ['deleted'])))
}

export async function getDynasty(req: express.Request, res: express.Response) {
    const item = await Dynasty.query().findById(req.params.id).where('deleted', false)
    res.status(200).send(_.omit(item, ['deleted']))
}

export async function createDynasty(req: express.Request, res: express.Response) {
    const { dynasty_name, owner_id } = req.body
    const newDynasty = await Dynasty.query().insert({
        dynasty_name,
        owner_id
    })
    res.status(201).send(_.omit(newDynasty, ['deleted']))
}