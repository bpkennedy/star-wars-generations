import express from 'express'
// @ts-ignore
import { noise } from '../noise'
import { Planet } from '../dao/planets-dao'
import { Ship } from '../dao/ships-dao'
import * as _ from 'lodash'

const GALAXY_SEED = 1337
noise.seed(GALAXY_SEED)

export async function getSystem(req: express.Request, res: express.Response) {
    const systemByPlanet: any = await Planet.query().findById(req.params.id).select(['id', 'name', 'x', 'y'])
        .where('deleted', false)
    systemByPlanet.tile_matrix = generateTileMatrix(systemByPlanet.x, systemByPlanet.y)
    res.status(200).send(systemByPlanet || {})
}

export async function getSystemShips(req: express.Request, res: express.Response) {
    const shipsByPlanet: any = await Ship.query().where('system_id', req.params.id).andWhere('deleted', false)
    res.status(200).send(shipsByPlanet.map((s: Ship) => _.omit(s, ['deleted'])) || [])
}

function generateTileMatrix(x: number, y: number): any {
    const noises = []
    for (let r = 0; r < 10; r++) {
        for (let c = 0; c < 10; c++) {
            noises.push(Math.abs(noise.simplex2(x * r / 100, y * c / 100)))
        }
    }
    const sortedNoises = [...noises]
    sortedNoises.sort()
    const highest = sortedNoises[sortedNoises.length - 1]
    const middle = sortedNoises[sortedNoises.length / 2]
    const finalMatrix = []
    for (let r = 0; r < 10; r++) {
        finalMatrix.push([])
        for (let c = 0; c < 10; c++) {
            const noisesIndex = r * 10 + c
            let tileValue = 0
            if (noises[noisesIndex] === highest) {
                tileValue = 2
            }
            if (noises[noisesIndex] === middle) {
                tileValue = 1
            }
            finalMatrix[r].push(tileValue)
        }
    }
    return finalMatrix
}