import express from 'express'
import * as _ from 'lodash'
import { Character } from '../dao/characters-dao'
import { Ship } from '../dao/ships-dao'

export async function getAllCharacters(req: express.Request, res: express.Response) {
    const items = await Character.query().where('deleted', false)
    res.status(200).send(items.map(i => _.omit(i, ['deleted'])))
}

export async function getUserCharacter(req: any, res: express.Response) {
    const character = await retrieveCharacterWithDynasty(req.user.sub)
    if (character) {
        res.status(200).send(_.omit(character, ['deleted']))
    } else {
        res.status(200).send(null)
    }
}

export async function retrieveCharacterWithDynasty(uid: string) {
    return Character.query().where('owner_id', uid).andWhere('deleted', false).withGraphFetched('[dynasty]').limit(1).first()
}

export async function getCharacter(req: express.Request, res: express.Response) {
    const item = await Character.query().findById(req.params.id).where('deleted', false).withGraphFetched('[dynasty]')
    res.status(200).send(_.omit(item, ['deleted']))
}

export async function updateCharacter(req: express.Request, res: express.Response) {
    const item = await Character.query().findById(req.params.id).where('deleted', false)
    if (!item) {
        res.status(404).send('Item by that ID Not Found')
    }
    const updatedItem = await Character.query().patchAndFetchById(req.params.id, _.omit(req.body, ['id']))
    res.status(200).send(_.omit(updatedItem, ['deleted']))
}

export async function createCharacter(req: express.Request, res: express.Response) {
    const { name, species, gender, pron_s, pron_o,
        pron_p, pron_ap, s_engine, s_navigation, s_weapons,
        s_shields, owner_id, dynasty_id, ship_id, interior_tile_id } = req.body
    const newCharacter: any = await Character.query().insert({
        name,
        species,
        gender,
        pron_s,
        pron_o,
        pron_p,
        pron_ap,
        s_engine,
        s_navigation,
        s_weapons,
        s_shields,
        owner_id,
        dynasty_id,
        ship_id,
        interior_tile_id
    })
    await createBeginnerShipForNewCharacter(newCharacter.id)
    const character = await Character.query().findById(newCharacter.id).where('deleted', false).withGraphFetched('[dynasty]')
    res.status(201).send(_.omit(character, ['deleted']))
}

async function createBeginnerShipForNewCharacter(newCharacterId: number) {
    const beginnerShip: any = await Ship.query().insert({
        ship_type: 'Z-95 Headhunter',
        class: 'fighter',
        rating: 3,
        owner_character_id: newCharacterId,
        system_id: 1,
    })
    return Character.query().findById(newCharacterId).patch({
        ship_id: beginnerShip.id,
    })
}