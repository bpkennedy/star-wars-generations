import express from 'express'
import * as _ from 'lodash'
import {updatePlanetAttributes} from './planets-api'
import {calculatePlanetProximities} from '../formulas'
import {Resource} from '../dao/resources-dao'
import {Planet} from '../dao/planets-dao'
import {Company} from '../dao/companies-dao'

export const MINIMUM_RESOURCES_FOR_COMPANIES = 1000

export async function getAllCompanies(req: express.Request, res: express.Response) {
    const items = await Company.query().where('deleted', false)
    res.status(200).send(items)
}

export async function updateCompanies(planets: Planet[]) {
    const companyFranchises = await Company.query()
        .where('deleted', false)

    for (const company of companyFranchises) {
        const myPlanet = planets.find(p => p.id === company.planet_id)
        const resourcesPlanets = filterAvailablePlanetsWithResource(company, planets)
        const resourcePlanetProximities = calculatePlanetProximities(myPlanet, resourcesPlanets)
        await expandFranchises(company, resourcePlanetProximities)
        await closeFranchises(company, myPlanet)
    }
}

async function closeFranchises(company: Company, myPlanet: any) {
    const myResources = myPlanet.resources.filter((r: Resource) => r.type === company.resource_type)
    for (const resource of myResources) {
        if (resource.quantity < MINIMUM_RESOURCES_FOR_COMPANIES) {
            await Company.query().findById(company.id).patch({
                deleted: true
            })
            await updatePlanetAttributes(myPlanet.id, false)
        }
    }
}

async function expandFranchises(company: Company, resourcePlanetProximities: any[]) {
    const nearestResourcePlanet = _.find(resourcePlanetProximities, (planetProximity => company.maximum_expansion_movement > planetProximity.distance))
    if (nearestResourcePlanet) {
        const parentCompany: Company = await Company.query().findById(company.id)
        await Company.query().insert({
            planet_id: nearestResourcePlanet.planet_id,
            name: parentCompany.name,
            resource_type: parentCompany.resource_type,
            maximum_expansion_movement: parentCompany.maximum_expansion_movement,
        })
        await updatePlanetAttributes(nearestResourcePlanet.planet_id, true)
    }
}

function filterAvailablePlanetsWithResource(company: Company, planets: any[]) {
    return planets.filter((p: any) => {
        const isResourceType = _.some(p.resources, ((r: Resource) => r.type === company.resource_type && r.quantity > MINIMUM_RESOURCES_FOR_COMPANIES))
        const isAlreadyFranchised = _.some(p.companies, ((c: Company) => c.name === company.name))
        return isResourceType && !isAlreadyFranchised
    })
}
