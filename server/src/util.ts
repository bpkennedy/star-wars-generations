export async function getItem(Model: any, id: number) {
    return Model.query().findById(id).where('deleted', false)
}

export function distanceBetweenTwoPoints(point1x: number, point1y: number, point2x: number, point2y: number) {
    const a = point2x - point1x
    const b = point2y - point1y
    return Math.sqrt( (a * a) + (b * b) )
}

export function roundToWholeNumber(item: any) {
    return parseInt(parseFloat(item).toFixed(0), 10)
}