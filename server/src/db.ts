import * as logger from 'loglevel'
// @ts-ignore
import knexMigrate from 'knex-migrate'
import {Model} from 'objection'
import Knex from 'knex'
import {Ship} from './dao/ships-dao'
import {Planet} from './dao/planets-dao'
import {Resource} from './dao/resources-dao'
import {Company} from './dao/companies-dao'
import {Dynasty} from './dao/dynasties-dao'
import {Character} from './dao/characters-dao'
import {Trip} from './dao/trips-dao'

const config = require('./config')
const pg = require('pg')

const PG_DECIMAL_OID = 1700
const PG_BIGINT_OID = 20
pg.types.setTypeParser(PG_DECIMAL_OID, parseFloat)
pg.types.setTypeParser(PG_BIGINT_OID, parseFloat)

const dataModels: any[] = [
    Ship,
    Planet,
    Resource,
    Company,
    Dynasty,
    Character,
    Trip,
]

export let knex: any

export async function setup(port: string) {
    knex = Knex({
        client: 'pg',
        version: '9.0',
        connection: {
            host: config.pgHost,
            port: parseInt(port, 10),
            user: config.pgUser,
            password: config.pgPassword,
            database: config.pgDatabase,
        },
        pool: {min: 2, max: 80},
    })
    Model.knex(knex)
    await knexMigrate('up', {}, logger.error)
}

export async function shutdown() {
    knex.destroy()
}

async function truncateTable(tableName: string) {
    if (tableName) {
        return knex.raw('truncate table "' + tableName + '" RESTART IDENTITY')
    }
}

export async function removeExistingData() {
    return Promise.all([
        ...dataModels.map(model => truncateTable(model.tableName)),
        knex.raw('truncate table "game_state" RESTART IDENTITY')
    ])
}
