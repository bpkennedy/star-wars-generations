import * as logger from 'loglevel'
import {GameState} from './dao/game-state-dao'
// @ts-ignore
import {Clock} from './clock'
import {raw} from 'objection'
import {updatePlanets} from './api/planets-api'
import {updateCompanies} from './api/companies-api'
import {Trip} from './dao/trips-dao'
import {io} from './socket'
import {expireCompletedTrips, updateTripPositions} from './api/trips-api'
import {updateShipPositions} from './api/ships-api'

export interface SimulationOptions {
    debug: boolean;
    tickInterval?: number;
}

function delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms))
}

export async function simulate(options: SimulationOptions) {
    if (options.debug === true) {
        logger.warn('Simulation run in DEBUG mode, tick interval set to ' + options.tickInterval)
        return
    }
    async function dayLoop() {
        logger.warn('Simulation run in PRODUCTION mode, tick interval set to ' + options.tickInterval)
        const oneDay = 86400000
        while (true) {
            await dailyTick()
            await delay(options.tickInterval || oneDay)
        }
    }
    async function interactionLoop() {
        logger.warn('Interaction game loop in PRODUCTION mode, tick interval set to ' + options.tickInterval)
        while (true) {
            await interactionTick()
            await delay(options.tickInterval || 5000)
        }
    }
    dayLoop()
    interactionLoop()
}

export async function interactionTick() {
    const activeTrips = await Trip.query().andWhere('deleted', false)
    const updatedTripPositions = updateTripPositions(activeTrips)
    await updateShipPositions(updatedTripPositions)
    emitUpdatedTripsEvent(updatedTripPositions, activeTrips.map((t: any) => t.system_id))
    await expireCompletedTrips()
}

function emitUpdatedTripsEvent(updatedTrips: any[], systemIdsWithActiveTrips: number[]) {
    for (const systemId of systemIdsWithActiveTrips) {
        const systemTrips = updatedTrips.filter((t: any) => t.system_id === systemId)
        if (systemTrips.length > 0) {
            io.sockets.in(systemId).emit('trips', systemTrips)
        }
    }
}

export async function dailyTick() {
    const planets = await updatePlanets()
    await updateCompanies(planets)
    return recordTick()
}

async function recordTick() {
    return GameState.query().patch({
        last_world_tick: Clock.currentDate(),
        current_tick: raw('current_tick + 1')
    }).findById(1)
}
