import * as server from './server'
import * as logger from 'loglevel'

process.on('unhandledRejection', (err: Error) => {
    logger.error(err)
})

server.start()
