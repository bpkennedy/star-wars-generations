require('dotenv').config()
import express from 'express'
import http from 'http'
// @ts-ignore
import {createHttpTerminator} from 'http-terminator'
import * as logger from 'loglevel'
import * as bodyParser from 'body-parser'
import cors from 'cors'
import { errors } from 'celebrate'
import {simulate, SimulationOptions} from './simulate'
import { routes } from './routes'
import * as db from './db'
import { initSockets } from './socket'

const config = require('./config')

let httpTerminator: any

export async function start(appPort: number = 3001, dbPort: string = config.pgPort, options: SimulationOptions = { debug: false }) {
    if (options.debug) {
        process.env.NODE_ENV = 'development'
    }

    await db.setup(dbPort)

    const app: any = express()
    app.use(cors())
    app.use(bodyParser.json())

    app.use('/api/v1/', routes(options))

    app.use(errors())

    const server = http.createServer(app)

    initSockets(server)

    server.listen(appPort)
    httpTerminator = createHttpTerminator({
        gracefulTerminationTimeout: 4000,
        server
    })
    logger.info(`Server running at: ${server.address()}`)

    simulate(options)
}

export async function stop() {
    await db.shutdown()
    return httpTerminator.terminate()
}
