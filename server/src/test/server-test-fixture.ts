import {start, stop} from '../server'
// @ts-ignore
import {restoreDatabase} from '../../migrations/seed-helper'
import {removeExistingData} from '../db'
import {Ship} from '../dao/ships-dao'
import axios from 'axios'
import * as sinon from 'sinon'
// @ts-ignore
import {Clock} from '../clock'
// @ts-ignore
import {DEBUG_USER_UID} from '../auth'

export const APP_PORT = 4000
export const DB_PORT = '25432'

let dateIsMocked: boolean

export const apiPath = `http://localhost:${APP_PORT}/api/v1/`

export async function apiGet(path: string) {
    return axios.get(apiPath + path)
}

export async function apiPost(path: string, body: any, user?: string) {
    return axios.post(apiPath + path, body, {
        headers: {
            testsub: user ? user : ''
        }
    })
}

export async function apiPut(path: string, body: any) {
    return axios.put(apiPath + path, body)
}

export async function apiDelete(path: string) {
    return axios.delete(apiPath + path)
}

export function mockCurrentDate(date: Date): string {
    if (!dateIsMocked) {
        sinon.stub(Clock, 'currentDate')
        dateIsMocked = true
    }
    // @ts-ignore
    Clock.currentDate['returns'](date)
    return date.toISOString()
}

export async function createTestCharacter(name: string, dynasty_name: string = 'testDynasty', owner_id: string = DEBUG_USER_UID) {
    const character: any = {
        name,
        species: 'human',
        gender: 'male',
        pron_s: 'he',
        pron_o: 'him',
        pron_p: 'his',
        pron_ap: 'his',
        s_engine: 10,
        s_navigation: 10,
        s_weapons: 8,
        s_shields: 8,
        owner_id,
        interior_tile_id: null,
    }
    const dynastyResponse = await apiPost('dynasties', {
        dynasty_name,
        owner_id,
    })
    const postResponse = await apiPost('characters', {
        ...character,
        ship_id: null,
        dynasty_id: dynastyResponse.data.id,
    })
    return postResponse.data
}

export function setupTest(test: any) {
    const timeout = 5000

    test.timeout(timeout)

    before(async () => {
        await start(APP_PORT, DB_PORT, {
            debug: true,
        })
    })

    beforeEach(async () => {
        await removeExistingData()
        const knex = Ship.knex()
        await restoreDatabase(knex)
    })

    afterEach(async () => {
        if (dateIsMocked) {
            dateIsMocked = false
            // @ts-ignore
            Clock.currentDate['restore']()
        }
    })

    after(async () => {
        await stop()
    })
}
