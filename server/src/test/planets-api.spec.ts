import {expect} from 'chai'
import * as _ from 'lodash'
import {apiDelete, apiGet, setupTest} from './server-test-fixture'
// @ts-ignore
import {planets, transformCanonicalToMapX, transformCanonicalToMapY} from '../../migrations/seed-helper'
import {dailyTick} from '../simulate'
import {distanceBetweenTwoPoints} from '../util'
import {
    calculateDecrementResourceQuantity,
    calculateIncrementResourceQuantity,
    calculatePopulation,
    calculateQualityOfLife
} from '../formulas'

describe('Planets API', function() {
    setupTest(this)

    it('should return initial planets', async () => {
        const planetsResponse = await apiGet('planets')
        expect(planetsResponse.status).to.eql(200)
        expect(planetsResponse.data.map((p: any) => _.omit(p, ['id', 'deleted']))).to.eql(planets)
    })

    it('should not return deleted planets', async () => {
        const planetsResponse = await apiGet('planets')
        expect(planetsResponse.data.length).to.eql(planets.length)
        const planetId = planetsResponse.data[0].id
        await apiDelete('planets/' + planetId)
        const updatedPlanets = await apiGet('planets')
        expect(updatedPlanets.status).to.eql(200)
        expect(updatedPlanets.data.length).to.eql(planets.length - 1)
    })

    it('should return 200 trying to get planet with id that does not exist', async () => {
        const item = await apiGet('planets/12345')
        expect(item.status).to.eql(200)
        expect(item.data).to.eql({})
    })

    it('should return 200 ok if trying to delete a nonexistent planet', async () => {
        const deletionResponse = await apiDelete('planets/' + 12345)
        expect(deletionResponse.status).to.eql(204)
    })

    it('should get planet with id 1 and it is Coruscant with resources and companies', async () => {
        const coruscant = await apiGet('planets/1')
        expect(coruscant.data).to.eql({
            id: 1,
            name: 'Coruscant',
            qol: 0.70,
            job_prospects: 0.90,
            population: 1000000000000,
            climate: 0.30,
            crime: 0.34,
            sector: "",
            region: "Core",
            resources: [
                {
                    id: 1,
                    planet_id: 1,
                    type: 'metals',
                    quantity: 10000
                }
            ],
            companies: [
                {
                    planet_id: 1,
                    name: 'Aratech',
                    resource_type: 'metals',
                    maximum_expansion_movement: 250
                }
            ],
            x: transformCanonicalToMapX(0),
            y: transformCanonicalToMapY(0),
            deleted: false,
        })
    })

    it('should grow and decrement planet resources each tick by resource change and company depletion', async () => {
        const companies = await apiGet('companies')
        const planet = await apiGet('planets/1')
        const planetResource = planet.data.resources[0]
        const planetClimate = planet.data.climate
        expect(planetResource).to.eql({
            id: 1,
            planet_id: 1,
            type: 'metals',
            quantity: 10000
        })
        await dailyTick()

        const response = await apiGet('planets/1')
        const updatedPlanetResource = response.data.resources[0]
        const updatedPlanetClimate = response.data.climate
        const incrementedAmount = calculateIncrementResourceQuantity(planetResource.quantity, planetClimate)
        const decrementAmount = calculateDecrementResourceQuantity(planetResource, planet.data.companies, companies.data)
        expect(updatedPlanetResource.quantity).to.eql(10000 + incrementedAmount - decrementAmount)
        await dailyTick()

        const updatedCompanies = await apiGet('companies')
        const response2 = await apiGet('planets/1')
        const updatedPlanetResource2 = response2.data.resources[0]
        const incrementedAmount2 = calculateIncrementResourceQuantity(updatedPlanetResource.quantity, updatedPlanetClimate)
        const decrementAmount2 = calculateDecrementResourceQuantity(updatedPlanetResource, response.data.companies, updatedCompanies.data)
        expect(updatedPlanetResource2.quantity).to.eql(updatedPlanetResource.quantity + incrementedAmount2 - decrementAmount2)
    })

    it('should grow planet companies each tick when within range of relevant resource type company', async () => {
        const coruscant = await apiGet('planets/1')
        const coruscantCompanies = coruscant.data.companies
        expect(coruscantCompanies).to.eql([{
            planet_id: 1,
            name: 'Aratech',
            resource_type: 'metals',
            maximum_expansion_movement: 250
        }])
        const closeEnoughPlanetWithResource = await apiGet('planets/3')
        const closeEnoughPlanetWithResourceCompanies = closeEnoughPlanetWithResource.data.companies
        const closeEnoughPlanetWithResourceResources = closeEnoughPlanetWithResource.data.resources
        expect(closeEnoughPlanetWithResourceCompanies).to.eql([])
        expect(closeEnoughPlanetWithResourceResources).to.eql([{
            id: 2,
            planet_id: 3,
            type: 'metals',
            quantity: 350000
        }])

        const tooFarPlanet = await apiGet('planets/2')
        const tooFarPlanetCompanies = tooFarPlanet.data.companies
        expect(distanceBetweenTwoPoints(coruscant.data.x, coruscant.data.y, tooFarPlanet.data.x, tooFarPlanet.data.y)).to.be.greaterThan(tooFarPlanetCompanies[0].maximum_expansion_movement)

        await dailyTick()

        const closeEnoughPlanetWithResource2 = await apiGet('planets/3')
        const closeEnoughPlanetWithResourceCompanies2 = closeEnoughPlanetWithResource2.data.companies
        expect(distanceBetweenTwoPoints(coruscant.data.x, coruscant.data.y, closeEnoughPlanetWithResource.data.x, closeEnoughPlanetWithResource.data.y)).to.be.lessThan(coruscantCompanies[0].maximum_expansion_movement)
        expect(closeEnoughPlanetWithResourceCompanies2).to.eql([{
            planet_id: 3,
            name: 'Aratech',
            resource_type: 'metals',
            maximum_expansion_movement: 250
        }])
    })

    it('should raise crime 5% and lower climate 2% when a company is added to a planet', async () => {
        const duro = await apiGet('planets/3')
        const duroInitialClimate = duro.data.climate
        const duroInitialCrime = duro.data.crime
        expect(duro.data.companies.length).to.eql(0)

        await dailyTick()

        const duroWithTwoCompanies = await apiGet('planets/3')
        expect(duroWithTwoCompanies.data.companies.length).to.eql(1)
        expect(duroWithTwoCompanies.data.climate).to.eql((duroInitialClimate - (duroInitialClimate * .02)))
        expect(duroWithTwoCompanies.data.crime).to.eql((duroInitialCrime + (duroInitialCrime * .05)))
    })

    it('should lower crime 5% and raise climate 2% when a company is removed from a planet', async () => {
        const tatooine = await apiGet('planets/2')
        const tatooineInitialClimate = tatooine.data.climate
        const tatooineInitialCrime = tatooine.data.crime
        expect(tatooine.data.companies.length).to.eql(1)

        await dailyTick()

        const tatooineWithNoCompanies = await apiGet('planets/2')
        expect(tatooineWithNoCompanies.data.companies.length).to.eql(0)
        expect(tatooineWithNoCompanies.data.climate).to.eql((tatooineInitialClimate + (tatooineInitialClimate * .02)))
        expect(tatooineWithNoCompanies.data.crime).to.eql((tatooineInitialCrime - (tatooineInitialCrime * .05)))
    })

    it('should allow companies to remove themselves from a planet after resources are drained to 1000', async () => {
        const tatooine = await apiGet('planets/2')
        expect(tatooine.data.resources).to.eql([{
            id: 3,
            planet_id: 2,
            type: 'metals',
            quantity: 200
        }])
        expect(tatooine.data.companies).to.eql([{
            planet_id: 2,
            name: 'Sienar Technologies',
            resource_type: 'metals',
            maximum_expansion_movement: 125
        }])

        await dailyTick()
        const tatooine2 = await apiGet('planets/2')
        expect(tatooine2.data.resources[0].quantity).to.be.lessThan(1000)
        expect(tatooine2.data.companies).to.eql([])
    })

    it('should increase Job Prospects by 2% and for a planet when a company is added', async () => {
        const duro = await apiGet('planets/3')
        const duroInitialJobProspects = duro.data.job_prospects
        expect(duro.data.companies.length).to.eql(0)

        await dailyTick()

        const duroWithOneCompany = await apiGet('planets/3')
        expect(duroWithOneCompany.data.companies.length).to.eql(1)
        expect(duroWithOneCompany.data.job_prospects).to.eql((duroInitialJobProspects + (duroInitialJobProspects * .02)))
    })

    it('should decrease Job Prospects by 2% and for a planet when a company is removed', async () => {
        const tatooine = await apiGet('planets/2')
        const tatooineInitialJobProspects = tatooine.data.job_prospects
        expect(tatooine.data.companies.length).to.eql(1)

        await dailyTick()

        const tatooineWithNoCompanies = await apiGet('planets/2')
        expect(tatooineWithNoCompanies.data.companies.length).to.eql(0)
        expect(tatooineWithNoCompanies.data.job_prospects).to.eql((tatooineInitialJobProspects - (tatooineInitialJobProspects * .02)))
    })

    it('should have Quality of Life approximated from climate, crime, and job prospects', async () => {
        await dailyTick()

        const duro2 = await apiGet('planets/3')
        const qol = calculateQualityOfLife({
            climate: duro2.data.climate,
            crime: duro2.data.crime,
            job_prospects: duro2.data.job_prospects
        })
        expect(duro2.data.qol).to.eql(qol)
    })

    it('should have population growth of base 1% plus the QoL as a fraction of 1%', async () => {
        const duro = await apiGet('planets/3')
        const duroInitialPopulation = duro.data.population

        await dailyTick()

        const duro2 = await apiGet('planets/3')
        const qol = calculateQualityOfLife({
            climate: duro2.data.climate,
            crime: duro2.data.crime,
            job_prospects: duro2.data.job_prospects
        })
        const expectedPopulation = calculatePopulation(duroInitialPopulation, qol)
        expect(duro2.data.population).to.eql(expectedPopulation)
    })
})
