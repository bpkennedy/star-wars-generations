import {expect} from 'chai'
import {apiGet, APP_PORT, DB_PORT, mockCurrentDate} from './server-test-fixture'
import {start, stop} from '../server'
// @ts-ignore
import {restoreDatabase} from '../../migrations/seed-helper'
import {removeExistingData} from '../db'
import {Ship} from '../dao/ships-dao'
import {dailyTick} from '../simulate'

function addOneDay(date: Date) {
    return new Date(date.setDate(date.getDate() + 1))
}

describe('Simulation', function() {
    it('should startup on a specific date', async () => {
        const exampleDate = new Date('2020-03-03')
        mockCurrentDate(exampleDate)

        await start(APP_PORT, DB_PORT, {
            debug: true,
        })
        await removeExistingData()
        const knex = Ship.knex()
        await restoreDatabase(knex)

        const response = await apiGet('heartbeat')

        expect(response.status).to.eql(200)
        expect(response.data.current_tick).to.eql(1)
        expect(new Date(response.data.last_world_tick)).to.eql(exampleDate)

        await stop()
    })

    it('can simulate 3 days of ticks', async () => {
        const today = new Date()
        mockCurrentDate(today)

        await start(APP_PORT, DB_PORT, {
            debug: true
        })
        await removeExistingData()
        const knex = Ship.knex()
        await restoreDatabase(knex)

        const response = await apiGet('heartbeat')
        expect(response.data.current_tick).to.eql(1)
        expect(new Date(response.data.last_world_tick)).to.eql(today)

        const secondDay = addOneDay(today)
        mockCurrentDate(secondDay)
        await dailyTick()

        const response2 = await apiGet('heartbeat')
        expect(response2.data.current_tick).to.eql(2)
        expect(new Date(response2.data.last_world_tick)).to.eql(secondDay)

        const thirdDay = addOneDay(secondDay)
        mockCurrentDate(thirdDay)
        await dailyTick()

        const response3 = await apiGet('heartbeat')
        expect(response3.data.current_tick).to.eql(3)
        expect(new Date(response3.data.last_world_tick)).to.eql(thirdDay)

        await stop()
    })
})
