import {expect} from 'chai'
import * as _ from 'lodash'
import {apiGet, apiPost, setupTest} from './server-test-fixture'

describe('Dynasties API', function() {
    setupTest(this)

    it('should create a dynasty', async () => {
        const dynasty = {
            dynasty_name: 'testDynasty',
            owner_id: 'someUserUid',
        }
        const postResponse = await apiPost('dynasties', dynasty)
        expect(postResponse.status).to.eql(201)
        expect(_.omit(postResponse.data, ['id'])).to.eql(dynasty)

        const response = await apiGet('dynasties')
        expect(response.status).to.eql(200)
        expect(response.data.map((p: any) => _.omit(p, ['id']))).to.eql([dynasty])
    })

    it('should get a dynasty', async () => {
        const dynasty = {
            dynasty_name: 'testDynasty',
            owner_id: 'someUserUid',
        }
        const dynastyResponse = await apiPost('dynasties', dynasty)
        const response = await apiGet('dynasties/' + dynastyResponse.data.id)
        expect(response.status).to.eql(200)
        expect(_.omit(response.data, ['id'])).to.eql(dynasty)
    })

    // TODO: dynasty name creation should be unique name
})
