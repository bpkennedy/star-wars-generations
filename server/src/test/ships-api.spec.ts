import * as _ from 'lodash'
import {expect} from 'chai'
import {apiDelete, apiGet, setupTest} from './server-test-fixture'
// @ts-ignore
import {ships} from '../../migrations/seed-helper'

describe('Ships API', function() {
    setupTest(this)

    it('should return initial ships', async () => {
        const shipsResponse = await apiGet('ships')
        expect(shipsResponse.status).to.eql(200)
        expect(shipsResponse.data.map((s: any) => _.omit(s, ['id', 'deleted']))).to.eql(ships)
    })

    it('should not return deleted ships', async () => {
        const shipsResponse = await apiGet('ships')
        expect(shipsResponse.data.length).to.eql(ships.length)
        const shipId = shipsResponse.data[0].id
        await apiDelete('ships/' + shipId)
        const updatedShips = await apiGet('ships')
        expect(updatedShips.status).to.eql(200)
        expect(updatedShips.data.length).to.eql(ships.length - 1)
    })

    it('should get ship with id 1 and it is z-95', async () => {
        const shipsResponse = await apiGet('ships/1')
        expect(shipsResponse.status).to.eql(200)
        expect(shipsResponse.data).to.eql({
            id: 1,
            ship_type: 'Z-95 Headhunter',
            class: 'fighter',
            rating: 3,
            deleted: false,
            owner_character_id: null,
            rotation: 0,
            system_id: null,
            system_x: 0,
            system_y: 0
        })
    })

    it('should return 200 trying to get ship with id that does not exist', async () => {
        const item = await apiGet('ships/12345')
        expect(item.status).to.eql(200)
        expect(item.data).to.eql({})
    })

    it('should return 200 ok if trying to delete a nonexistent ship', async () => {
        const deletionResponse = await apiDelete('ships/' + 12345)
        expect(deletionResponse.status).to.eql(204)
    })
})
