import {expect} from 'chai'
import * as _ from 'lodash'
import {apiGet, apiPost, apiPut, setupTest} from './server-test-fixture'
// @ts-ignore
import { planets, ships } from '../../migrations/seed-helper'
// @ts-ignore
import {DEBUG_USER_UID} from '../auth'

describe('Character API', function() {
    setupTest(this)

    it('should create a character in a dynasty', async () => {
        const character: any = {
            name: 'Domingo Splatt',
            species: 'human',
            gender: 'male',
            pron_s: 'he',
            pron_o: 'him',
            pron_p: 'his',
            pron_ap: 'his',
            s_engine: 10,
            s_navigation: 10,
            s_weapons: 8,
            s_shields: 8,
            owner_id: 'someUserUid',
            interior_tile_id: null,
        }
        const dynastyResponse = await apiPost('dynasties', {
            dynasty_name: 'testDynasty',
            owner_id: 'someUserUid',
        })
        const postResponse = await apiPost('characters', {
            ...character,
            ship_id: ships.length + 1,
            dynasty_id: dynastyResponse.data.id,
        })
        expect(postResponse.status).to.eql(201)
        expect(_.omit(postResponse.data, ['id'])).to.eql({
            ...character,
            ship_id: ships.length + 1,
            dynasty_id: dynastyResponse.data.id,
            dynasty: {
                dynasty_name: 'testDynasty'
            }
        })

        const response = await apiGet('characters')
        expect(response.status).to.eql(200)
        expect(response.data.map((p: any) => _.omit(p, ['id', 'dynasty_id', 'dynasty_name']))).to.eql([
            {...character, ship_id: ships.length + 1}
        ])
    })

    it('should create a corresponding ship in coruscant system during character creation', async () => {
        const character: any = {
            name: 'Domingo Splatt',
            species: 'human',
            gender: 'male',
            pron_s: 'he',
            pron_o: 'him',
            pron_p: 'his',
            pron_ap: 'his',
            s_engine: 10,
            s_navigation: 10,
            s_weapons: 8,
            s_shields: 8,
            owner_id: 'someUserUid',
            interior_tile_id: null,
        }
        const dynastyResponse = await apiPost('dynasties', {
            dynasty_name: 'testDynasty',
            owner_id: 'someUserUid',
        })
        const postResponse = await apiPost('characters', {
            ...character,
            ship_id: null,
            dynasty_id: dynastyResponse.data.id,
        })
        const characterShip = await apiGet('/ships/' + postResponse.data.ship_id)
        expect(characterShip.data.owner_character_id).to.eql(postResponse.data.id)
        const dbPlanets = await apiGet('/planets')
        const coruscant = dbPlanets.data.find((p: any) => p.name === 'Coruscant')
        expect(characterShip.data.system_id).to.eql(coruscant.id)
    })

    it('should get a specific character', async () => {
        const character: any = {
            name: 'Domingo Splatt',
            species: 'human',
            gender: 'male',
            pron_s: 'he',
            pron_o: 'him',
            pron_p: 'his',
            pron_ap: 'his',
            s_engine: 10,
            s_navigation: 10,
            s_weapons: 8,
            s_shields: 8,
            owner_id: 'someUserUid',
            interior_tile_id: null,
        }
        const dynastyResponse = await apiPost('dynasties', {
            dynasty_name: 'testDynasty',
            owner_id: 'someUserUid',
        })
        const postResponse = await apiPost('characters', {...character, dynasty_id: dynastyResponse.data.id, ship_id: null})
        const characterResponse = await apiGet('characters/' + postResponse.data.id)
        expect(characterResponse.status).to.eql(200)
        expect(_.omit(characterResponse.data, ['id', 'dynasty', 'ship_id'])).to.eql({...character, dynasty_id: dynastyResponse.data.id})
    })

    it('should get a user\'s character', async () => {
        const character: any = {
            name: 'Domingo Splatt',
            species: 'human',
            gender: 'male',
            pron_s: 'he',
            pron_o: 'him',
            pron_p: 'his',
            pron_ap: 'his',
            s_engine: 10,
            s_navigation: 10,
            s_weapons: 8,
            s_shields: 8,
            owner_id: DEBUG_USER_UID,
            interior_tile_id: null,
        }
        const dynastyResponse = await apiPost('dynasties', {
            dynasty_name: 'testDynasty',
            owner_id: DEBUG_USER_UID
        })
        await apiPost('characters', {...character, dynasty_id: dynastyResponse.data.id, ship_id: null})
        const characterResponse = await apiGet('character')
        expect(characterResponse.status).to.eql(200)
        expect(_.omit(characterResponse.data, ['id', 'dynasty', 'ship_id'])).to.eql({...character, dynasty_id: dynastyResponse.data.id})
    })

    it('should be able to update character pronouns', async () => {
        const character: any = {
            name: 'Domingo Splatt',
            species: 'human',
            gender: 'male',
            pron_s: 'he',
            pron_o: 'him',
            pron_p: 'his',
            pron_ap: 'his',
            s_engine: 10,
            s_navigation: 10,
            s_weapons: 8,
            s_shields: 8,
            owner_id: 'someUserUid',
            interior_tile_id: null,
            ship_id: null,
        }
        const dynastyResponse = await apiPost('dynasties', {
            dynasty_name: 'testDynasty',
            owner_id: 'someUserUid',
        })
        const postResponse = await apiPost('characters', {...character, dynasty_id: dynastyResponse.data.id})
        await apiPut('characters/' + postResponse.data.id, {
            gender: 'other',
            pron_s: 'they',
            pron_o: 'them',
            pron_p: 'their',
            pron_ap: 'theirs',
        })
        const updatedCharacterResponse = await apiGet('characters/' + postResponse.data.id)
        expect(updatedCharacterResponse.data.name).to.eql('Domingo Splatt')
        expect(updatedCharacterResponse.data.gender).to.eql('other')
        expect(updatedCharacterResponse.data.pron_s).to.eql('they')
        expect(updatedCharacterResponse.data.pron_o).to.eql('them')
        expect(updatedCharacterResponse.data.pron_p).to.eql('their')
        expect(updatedCharacterResponse.data.pron_ap).to.eql('theirs')
    })
})
