import { expect } from 'chai'
import * as _ from 'lodash'
import {apiGet, apiPost, setupTest} from './server-test-fixture'
// @ts-ignore
import { transformCanonicalToMapX, transformCanonicalToMapY } from '../../migrations/seed-helper'

describe('Systems API', function() {
    setupTest(this)

    it('should return coruscant system with generated, repeatable tile matrix', async () => {
        const systemResponse = await apiGet('systems/1')
        expect(systemResponse.status).to.eql(200)
        expect(systemResponse.data).to.eql({
            id: 1,
            name: 'Coruscant',
            x: transformCanonicalToMapX(0),
            y: transformCanonicalToMapY(0),
            tile_matrix: [
                [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
                [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
                [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
                [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
                [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
                [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
                [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
                [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
                [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
                [ 0, 0, 0, 2, 1, 0, 0, 0, 0, 0 ],
            ]
        })
    })

    it('should return other player ships in same system', async () => {
        const character: any = {
            name: 'Domingo Splatt',
            species: 'human',
            gender: 'male',
            pron_s: 'he',
            pron_o: 'him',
            pron_p: 'his',
            pron_ap: 'his',
            s_engine: 10,
            s_navigation: 10,
            s_weapons: 8,
            s_shields: 8,
            owner_id: 'someUserUid',
            interior_tile_id: null,
        }
        const dynastyResponse = await apiPost('dynasties', {
            dynasty_name: 'testDynasty',
            owner_id: 'someUserUid',
        })
        await apiPost('characters', {
            ...character,
            ship_id: null,
            dynasty_id: dynastyResponse.data.id,
        })
        const dynastyResponse2 = await apiPost('dynasties', {
            dynasty_name: 'testDynasty2',
            owner_id: 'someUserUid2',
        })
        await apiPost('characters', {
            ...character,
            name: 'BaBringo Splattz',
            owner_id: 'someUserUid2',
            ship_id: null,
            dynasty_id: dynastyResponse2.data.id,
        })


        const systemShipsResponse = await apiGet('systems/1/ships')
        expect(systemShipsResponse.status).to.eql(200)
        expect(systemShipsResponse.data.map((s: any) => s.system_id)).to.eql([1, 1])
    })
})
