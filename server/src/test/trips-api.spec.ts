import {expect} from 'chai'
import {apiGet, apiPost, createTestCharacter, mockCurrentDate, setupTest} from './server-test-fixture'
import {Trip} from '../dao/trips-dao'
import {interactionTick} from '../simulate'

describe('Trips API', function() {
    setupTest(this)

    it('should create a trip for a character in a ship in a system', async () => {
        const testChar = await createTestCharacter('Domingo Splatt')
        const characterShipResponse = await apiGet('/ships/' + testChar.ship_id)
        const shipSystemId = characterShipResponse.data.system_id

        const tripStartDate = mockCurrentDate(new Date())
        const pathToTravel = [[0,0],[0,1],[0,2],[0,3]]
        const newTripResponse = await apiPost('trips', {
            path: pathToTravel
        })
        expect(newTripResponse.data.start_date).to.eql(tripStartDate)
        expect(newTripResponse.data.system_id).to.eql(shipSystemId)
        expect(newTripResponse.data.ship_id).to.eql(testChar.ship_id)
        expect(newTripResponse.data.path).to.eql(pathToTravel)
    })

    it('should get all system trips by system', async () => {
        const testChar1 = await createTestCharacter('Domingo Splatt')
        const testChar2 = await createTestCharacter('Johnny Hotbody', 'testDynasty2', 'someUid')
        const char1ShipResponse = await apiGet('/ships/' + testChar1.ship_id)
        const char2ShipResponse = await apiGet('/ships/' + testChar2.ship_id)
        const ship1SystemId = char1ShipResponse.data.system_id
        const ship2SystemId = char2ShipResponse.data.system_id
        expect(ship1SystemId).to.eql(ship2SystemId)

        const tripStartDate = mockCurrentDate(new Date())

        const char1PathToTravel = [[0,0],[0,1],[0,2],[0,3]]
        const char2PathToTravel = [[0,0],[1,0],[1,1],[2,1]]

        await apiPost('trips', { path: char1PathToTravel })
        await apiPost('trips', { path: char2PathToTravel }, 'someUid')

        const systemTripsResponse = await apiGet(`systems/${ship1SystemId}/trips`)
        expect(systemTripsResponse.data.length).to.eql(2)
        expect(systemTripsResponse.data.map((t: Trip) => t.ship_id)).to.eql([testChar1.ship_id, testChar2.ship_id])
        expect(systemTripsResponse.data.map((t: Trip) => t.system_id)).to.eql([ship1SystemId, ship2SystemId])
    })

    it('should delete trip after trip is complete', async () => {
        const testChar = await createTestCharacter('Domingo Splatt')
        const characterShipResponse = await apiGet('/ships/' + testChar.ship_id)
        const shipSystemId = characterShipResponse.data.system_id

        mockCurrentDate(new Date('2020-03-03'))
        await apiPost('trips', { path: [[0,0],[0,1],[0,2],[0,3]] })
        mockCurrentDate(new Date('2020-03-04'))
        await interactionTick()

        const systemTripsResponse = await apiGet(`systems/${shipSystemId}/trips`)
        expect(systemTripsResponse.data.length).to.eql(0)
    })
    // TODO make test that gets ALL active trips not just trips in a single system
    // ^-- requires hyperjumping between systems feature
})
