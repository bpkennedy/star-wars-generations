import {Model} from 'objection'

export class Trip extends Model {
    public static get tableName() {
        return 'trips'
    }

    id: number
    path: any[]
    start_date: Date
    destination_date: Date
    ship_id: number
    system_id: number
    deleted: boolean
}
