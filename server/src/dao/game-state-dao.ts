import {Model} from 'objection'

export class GameState extends Model {
    public static get tableName() {
        return 'game_state'
    }

    id: number
    status: string
    last_world_tick: Date
    current_tick: number
}
