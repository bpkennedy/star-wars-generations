import {Model} from 'objection'

export class Company extends Model {
    public static get tableName() {
        return 'companies'
    }

    id: number
    planet_id: number
    name: string
    resource_type: string
    maximum_expansion_movement: number
    deleted: boolean
}
