import {Model} from 'objection'

export class Dynasty extends Model {
    public static get tableName() {
        return 'dynasties'
    }

    id: number
    dynasty_name: string
    owner_id: string
    deleted: boolean
}
