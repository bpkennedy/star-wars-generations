import {Model} from 'objection'
import {Dynasty} from './dynasties-dao'

export class Character extends Model {
    public static get tableName() {
        return 'characters'
    }

    static relationMappings = {
        dynasty: {
            relation: Model.BelongsToOneRelation,
            modelClass: Dynasty,
            filter: (query: any) => query.select('dynasty_name'),
            join: {
                from: 'characters.dynasty_id',
                to: 'dynasties.id'
            }
        }
    }

    id: number
    name: string
    species: string
    gender: string
    pron_s: string
    pron_o: string
    pron_p: string
    pron_ap: string
    s_engine: number
    s_navigation: number
    s_weapons: number
    s_shields: number
    dynasty_id: number
    owner_id: string
    deleted: boolean
    interior_tile_id: number
    ship_id: number
}
