import {Model} from 'objection'
import {Resource} from './resources-dao'
import {Company} from './companies-dao'

export class Planet extends Model {
    public static relationMappings = {
        resources: {
            relation: Model.HasManyRelation,
            modelClass: Resource,
            filter: (query: any) => query.select('id', 'planet_id', 'type', 'quantity').where('deleted', false),
            join: {
                from: 'planets.id',
                to: 'resources.planet_id'
            },
        },
        companies: {
            relation: Model.HasManyRelation,
            modelClass: Company,
            filter: (query: any) => query.select('planet_id', 'name', 'resource_type', 'maximum_expansion_movement').where('deleted', false),
            join: {
                from: 'planets.id',
                to: 'companies.planet_id'
            },
        }
    }
    public static get tableName() {
        return 'planets'
    }

    id: number
    name: string
    qol: number
    job_prospects: number
    population: number
    climate: number
    crime: number
    deleted: boolean
    x: number
    y: number
}
