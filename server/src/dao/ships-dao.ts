import {Model} from 'objection'

export class Ship extends Model {
    public static get tableName() {
        return 'ships'
    }

    id: number
    ship_type: string
    class: string
    rating: number
    owner_character_id: number
    rotation: number
    system_id: number
    system_x: number
    system_y: number
    deleted: boolean
}
