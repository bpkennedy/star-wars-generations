import {Model} from 'objection'

export class Resource extends Model {
    public static get tableName() {
        return 'resources'
    }

    id: number
    planet_id: number
    type: string
    quantity: number
    deleted: boolean
}
