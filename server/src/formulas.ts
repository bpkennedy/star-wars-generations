import {distanceBetweenTwoPoints, roundToWholeNumber} from './util'
import {Resource} from './dao/resources-dao'
import {Company} from './dao/companies-dao'
import {Planet} from './dao/planets-dao'
import * as _ from 'lodash'

const BASE_RESOURCE_GROWTH = .04
const baseClimateModifier = (climate: number) => climate / 10

export function calculateIncrementResourceQuantity(oldCount: number, climate: number) {
    return roundToWholeNumber(oldCount * (BASE_RESOURCE_GROWTH + baseClimateModifier(climate)))
}

export function calculateDecrementResourceQuantity(resource: Resource, planetCompanies: Company[], allCompanyFranchises: Company[]) {
    let resourcesToRemove = 0
    for (const company of planetCompanies) {
        if (company.resource_type === resource.type) {
            const companyFranchiseCount = allCompanyFranchises.filter(c => c.name === company.name).length
            resourcesToRemove += roundToWholeNumber(10 * companyFranchiseCount)
        }
    }
    return resourcesToRemove
}

export function calculatePopulation(population: number, qol: number) {
    return Math.floor(population + (population * (.01 + (qol/100))))
}

export function calculateQualityOfLife(planet: { climate: number, crime: number, job_prospects: number }) {
    return (planet.climate + (1 - planet.crime) + planet.job_prospects) / 3
}

export function calculatePlanetProximities(sourcePlanet: Planet, planets: any[]) {
    const filteredPlanets = planets.map(p => ({
        planet_id: p.id,
        distance: distanceBetweenTwoPoints(sourcePlanet.x, sourcePlanet.y, p.x, p.y)
    }))
    return _.sortBy(filteredPlanets, ['distance'])
}