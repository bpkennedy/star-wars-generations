// @ts-ignore
import { checkJwt, checkScopes } from './auth.js'
import * as express from 'express'
import { celebrate, Joi, Segments } from 'celebrate'
import {deleteShip, getAllShips, getShip} from './api/ships-api'
import {deletePlanet, getAllPlanets, getPlanet} from './api/planets-api'
import {getHeartbeat} from './api/heartbeat-api'
import {getAllCompanies} from './api/companies-api'
import {SimulationOptions} from './simulate'
import {createDynasty, getAllDynasties, getDynasty} from './api/dynasties-api'
import {createCharacter, getAllCharacters, getCharacter, updateCharacter, getUserCharacter} from './api/character-api'
import {getSystem, getSystemShips} from './api/systems-api'
import {createTrip, getSystemTrips} from './api/trips-api'

export const routes = (options: SimulationOptions) => {
    const router = express.Router()

    router.get('/test', (req, res) => {
        res.send('Working!')
    })

    router.get('/heartbeat', checkJwt(options), checkScopes(options), getHeartbeat)
    router.get('/ships', checkJwt(options), checkScopes(options), getAllShips)
    router.get('/ships/:id', checkJwt(options), checkScopes(options), celebrate({
        [Segments.PARAMS]: Joi.object().keys({
            id: Joi.string().required(),
        })
    }), getShip)
    router.delete('/ships/:id', checkJwt(options), checkScopes(options), celebrate({
        [Segments.PARAMS]: Joi.object().keys({
            id: Joi.string().required(),
        })
    }), deleteShip)
    router.get('/planets', checkJwt(options), checkScopes(options), getAllPlanets)
    router.get('/planets/:id', checkJwt(options), checkScopes(options), celebrate({
        [Segments.PARAMS]: Joi.object().keys({
            id: Joi.string().required(),
        })
    }), getPlanet)
    router.delete('/planets/:id', checkJwt(options), checkScopes(options), celebrate({
        [Segments.PARAMS]: Joi.object().keys({
            id: Joi.string().required(),
        })
    }), deletePlanet)
    router.get('/systems/:id', checkJwt(options), checkScopes(options), celebrate({
        [Segments.PARAMS]: Joi.object().keys({
            id: Joi.string().required(),
        })
    }), getSystem)
    router.get('/systems/:id/ships', checkJwt(options), checkScopes(options), celebrate({
        [Segments.PARAMS]: Joi.object().keys({
            id: Joi.string().required(),
        })
    }), getSystemShips)
    router.get('/systems/:id/trips', checkJwt(options), checkScopes(options), celebrate({
        [Segments.PARAMS]: Joi.object().keys({
            id: Joi.string().required(),
        })
    }), getSystemTrips)
    router.get('/companies', checkJwt(options), checkScopes(options), getAllCompanies)
    router.get('/dynasties', checkJwt(options), checkScopes(options), getAllDynasties)
    router.get('/dynasties/:id', checkJwt(options), checkScopes(options), celebrate({
        [Segments.PARAMS]: Joi.object().keys({
            id: Joi.string().required(),
        })
    }), getDynasty)
    router.post('/dynasties', checkJwt(options), checkScopes(options), celebrate({
        [Segments.BODY]: Joi.object().keys({
            dynasty_name: Joi.string().required(),
            owner_id: Joi.string().required()
        })
    }), createDynasty)
    router.get('/character', checkJwt(options), checkScopes(options), getUserCharacter)
    router.get('/characters', checkJwt(options), checkScopes(options), getAllCharacters)
    router.get('/characters/:id', checkJwt(options), checkScopes(options), celebrate({
        [Segments.PARAMS]: Joi.object().keys({
            id: Joi.string().required(),
        })
    }), getCharacter)
    router.put('/characters/:id', checkJwt(options), checkScopes(options), celebrate({
        [Segments.PARAMS]: Joi.object().keys({
            id: Joi.string().required(),
        }),
        [Segments.BODY]: Joi.object().keys({
            gender: Joi.string().optional(),
            pron_s: Joi.string().optional(),
            pron_o: Joi.string().optional(),
            pron_p: Joi.string().optional(),
            pron_ap: Joi.string().optional(),
        }),
    }), updateCharacter)
    router.post('/characters', checkJwt(options), checkScopes(options), celebrate({
        [Segments.BODY]: Joi.object().keys({
            name: Joi.string().required(),
            species: Joi.string().required(),
            gender: Joi.string().required(),
            pron_s: Joi.string().required(),
            pron_o: Joi.string().required(),
            pron_p: Joi.string().required(),
            pron_ap: Joi.string().required(),
            s_engine: Joi.number().required(),
            s_navigation: Joi.number().required(),
            s_weapons: Joi.number().required(),
            s_shields: Joi.number().required(),
            dynasty_id: Joi.number().required(),
            owner_id: Joi.string().required(),
            ship_id: Joi.number().required().allow(null),
            interior_tile_id: Joi.number().required().allow(null)
        })
    }), createCharacter)
    router.post('/trips', checkJwt(options), checkScopes(options), celebrate({
        [Segments.BODY]: Joi.object().keys({
            path: Joi.array().items(Joi.array().items(Joi.number().required()))
        })
    }), createTrip)
    return router
}
