const jwt = require('express-jwt');
const jwtAuthz = require('express-jwt-authz');
const jwksRsa = require('jwks-rsa');

const DEBUG_USER_UID = 'auth0|5f334e387ee0f0003d8ce5a3'
const DEBUG_USER = {
    iss: 'https://dev-a4gfqbch.us.auth0.com/',
    sub: DEBUG_USER_UID,
    aud: [
        'starwarsdynastiesapi',
        'https://dev-a4gfqbch.us.auth0.com/userinfo'
    ],
    iat: 1598448079,
    exp: 1598534479,
    azp: 'z7qPtVTYQb29IQ0HLOLyfWzITcVv0nJN',
    scope: 'openid profile email read:all'
}

// Authentication middleware. When used, the
// Access Token must exist and be verified against
// the Auth0 JSON Web Key Set
const checkJwt = (options) => {
    if (options.debug) {
        return function(req, res, next) {
            if (req.headers.testsub && req.headers.testsub.length > 0) {
                req.user = {...DEBUG_USER, sub: req.headers.testsub}
            } else {
                req.user = DEBUG_USER
            }
            next()
        }
    }
    return jwt({
        // Dynamically provide a signing key
        // based on the kid in the header and
        // the signing keys provided by the JWKS endpoint.
        secret: jwksRsa.expressJwtSecret({
            cache: true,
            rateLimit: true,
            jwksRequestsPerMinute: 5,
            jwksUri: `https://dev-a4gfqbch.us.auth0.com/.well-known/jwks.json`
        }),

        // Validate the audience and the issuer.
        audience: 'starwarsdynastiesapi',
        issuer: `https://dev-a4gfqbch.us.auth0.com/`,
        algorithms: ['RS256']
    })
};

const checkScopes = (options) => {
    if (options.debug) {
        return function(req, res, next) {
            next()
        }
    }
    return jwtAuthz(['read:all'])
}

module.exports = {
    checkJwt,
    checkScopes,
    DEBUG_USER_UID
};
