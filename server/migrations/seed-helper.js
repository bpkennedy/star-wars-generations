const _ = require('lodash')
const { Clock } = require('../src/clock.js')

const X_OFFSET = -47
const Y_OFFSET = -140

const ships = [{
        ship_type: 'Z-95 Headhunter',
        rating: 3,
        class: 'fighter',
        owner_character_id: null,
        system_id: null,
        system_x: 0,
        system_y: 0,
        rotation: 0,
    }, {
        ship_type: 'Eta-5 Interceptor',
        rating: 5,
        class: 'fighter',
        owner_character_id: null,
        system_id: null,
        system_x: 0,
        system_y: 0,
        rotation: 0,
}]

const resources = [{
    planet_id: 1,
    type: 'metals',
    quantity: 10000
}, {
    planet_id: 3,
    type: 'metals',
    quantity: 350000
}, {
    planet_id: 2,
    type: 'metals',
    quantity: 200
}]

const companies = [{
    planet_id: 1,
    name: 'Aratech',
    resource_type: 'metals',
    maximum_expansion_movement: 250,
}, {
    planet_id: 2,
    name: 'Sienar Technologies',
    resource_type: 'metals',
    maximum_expansion_movement: 125,
}]

const planets = [{
    name: 'Coruscant',
    qol: 0.70,
    job_prospects: 0.90,
    population: 1000000000000,
    climate: 0.30,
    crime: 0.34,
    x: transformCanonicalToMapX(0),
    y: transformCanonicalToMapY(0),
    sector: '',
    region: 'Core',
}, {
    name: 'Tatooine',
    qol: 0.10,
    job_prospects: 0.30,
    population: 200000,
    climate: 0.20,
    crime: 0.75,
    x: transformCanonicalToMapX(644.386),
    y: transformCanonicalToMapY(-673.274),
    sector: 'Arkanis',
    region: 'Outer Rim',
}, {
    name: 'Duro',
    qol: 0.23,
    job_prospects: 0.45,
    population: 18500000000,
    climate: 0.15,
    crime: 0.30,
    x: transformCanonicalToMapX(157.102),
    y: transformCanonicalToMapY(-186.797),
    sector: '',
    region: 'Core',
}]

function transformCanonicalToMapX(xCoord) {
    return xCoord + X_OFFSET
}

function transformCanonicalToMapY(yCoord) {
    return yCoord > 0 ? -Math.abs(yCoord) + Y_OFFSET : Math.abs(yCoord) + Y_OFFSET
}

async function createPlanets(knex) {
    if (process.env.NODE_ENV === 'development') {
        for (const planet of planets) {
            await knex('planets').insert(planet)
        }
    } else {
        const jsonPlanets = require('../src/planets_new.json')
        for (const planet of _.uniqBy(jsonPlanets.rows, 'planet')) {
            if (planet.x && planet.y) {
                await knex('planets').insert({
                    name: planet.planet,
                    qol: 0.23,
                    job_prospects: 0.45,
                    population: 18500000000,
                    climate: 0.15,
                    crime: 0.30,
                    x: transformCanonicalToMapX(planet.x),
                    y: transformCanonicalToMapY(planet.y),
                    sector: planet.sector,
                    region: planet.region,
                })
            }
        }
    }
}

async function seedDatabase(knex) {
    try {
        for (const ship of ships) {
            await knex('ships').insert(ship)
        }
        await createPlanets(knex)
        for (const resource of resources) {
            await knex('resources').insert(resource)
        }
        for (const company of companies) {
            await knex('companies').insert(company)
        }
        await knex('game_state').insert({
            status: 'running',
            last_world_tick: Clock.currentDate(),
        })
    } catch (error) {
        console.error('database seeding error:', error)
        throw error
    }
}

async function restoreDatabase(knex) {
    await seedDatabase(knex)
}

module.exports = {
    seedDatabase,
    restoreDatabase,
    transformCanonicalToMapX,
    transformCanonicalToMapY,
    planets,
    companies,
    resources,
    ships,
}

