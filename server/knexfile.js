const config = require('./src/config')

const knexConfig = {
  client: 'pg',
  version: '9.0',
  connection: {
    host: config.pgHost,
    port: parseInt(config.pgPort, 10),
    user: config.pgUser,
    password: config.pgPassword,
    database: config.pgDatabase,
  },
  pool: {min: 2, max: 80},
}

module.exports = {
  development: knexConfig,
  production: knexConfig,
};
